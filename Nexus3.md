# Crystal for Nexus 3 #

The Crystal packages had a number of things in them to sneak some additional functionality that Nexus 2 didn't offer natively. But pushing the boundaries of what you're supposed to do can cause problems. In particular, Crystal relied on a package I found online, written by one Samuel D. Corbin of Achaea, that allowed multi-colored displays, that I then extended to also support multiple lines with color-coding. Corbin's code did some tunneling into Nexus's internals that, in Nexus 3, not only didn't work, but once it was in place, Nexus 3 became non-responsive, refusing to even let you type commands.

The version 2 packages of Crystal resolve this problem and a number of others, and avoid using such deep trickery (now that Nexus 3 provides a supported method of displaying multi-color and formatted lines, that is!). If you start from a blank profile and follow the directions in [the main README](https://bitbucket.org/HawthornThistleberry/crystalship/src/master/README.md), everything should work fine. However, if you were already using Crystal in Nexus 2 and want to move to Nexus 3, you have a few options.

The key question here is, did you already fire up Nexus 3? And if so, how current were your Crystal installs?

## I haven't tried Nexus 3 yet ##

You might have made this easier for yourself. In this case, upgrade all the Crystal packages you have to the latest version first, following the usual process. Once that's done, and everything is just how you like it, you should be free to run Nexus 3 and let it bring over all your settings, including all of Crystal, and be up and running.

## I already tried Nexus 3 ##

The first time you fire up Nexus 3 and log into your character, it makes a copy of your complete set of settings, including all the packages you have. Which means the first time you fired up Nexus 3, you got a copy of whatever Crystal packages you had installed, and if those were pre-2.0 versions, your Nexus 3 is non-responsive. You log in and can type commands but it never executes them. This puts you in a sticky position! Here's how to get out of it.

1. Click the gear icon, go to Reflex Packages, and remove all the Crystal packages. Don't worry, your settings and data will not be lost.
2. Go to Reflexes and make any change to anything. You can change something and change it back. The point of this is just to force Nexus 3 to save, because deleting packages never does, and you can't quit to force a save that way.
3. Hit Refresh on your browser, and accept the warning that you're going to be logging off without saving.
4. Log in again. This time, though all of Crystal is missing (and any root package stuff you have that calls it might be giving you error messages), at least you can type.
5. Now install the new CrystalNotices.
6. You can verify that it is working by typing `cupdates` and you'll be told everything is up to date.
7. Next install the new CrystalQueues. You can verify it by typing `cq help`.
8. Now you can install any other Crystal packages you're using.

# It's not working! #

There was a lot of rewriting and tweaking necessary to get to this point. I haven't tested everything anywhere nearly as thoroughly as it had been previously tested. It's likely there will be quirks and errors here and there. The most likely things are:

* Displayed lines that are cut off very early
* Names missing or not being recognized

Whatever problems you find, please report them! The best places would be in the [Nexus discord](https://discord.gg/czQeH5yAUT), Lusternia or Serenwilde discord's Nexus channels, or in an OOC message to Lendren. Please be complete: which package you're using, what you were doing, what didn't work, screenshots or logs can be helpful. You can also report your bugs here: [Issues](https://bitbucket.org/HawthornThistleberry/crystalship/issues)

# Other Nexus 3 advice #

One thing I ran into a lot was my `onLoad` being too full of activity and Nexus 3 tending to ignore most of it, or run different onLoads in a different order. To get around this, I moved most of it to a trigger on the string `Your last login was from domain:` and spaced some of it out with setTimeout() calls (you could also use simple scripting's Wait action).

# Future Plans #

A few new Nexus 3 features need to be better used by Crystal:

* There's now official support for onInstall, which means I can probably get rid of the `cinstall` command entirely.
* There's also an onUninstall function. I can make Crystal clean up after itself when it's removed, though I have to see if that would also include when you upgrade a package.

