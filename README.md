# Purpose #

This repository started as a home for CrystalShip, a Nexus package to support Lusternia aetherhunting (hence the repository name), but it has now expanded to include other Nexus packages by Lendren Starfall. All these packages will automatically notify you if there is an update available. **Scroll down for instructions for downloading, installing, updating, and bug-reporting.** Here's what it includes:

* Hunting, influencing, aetherhunting (all seats), and debating
* Defense profiles and upkeep (not complete)
* Faction-based name coloring and name lists
* Map improvements, clickable travel links, waypoints
* Shopkeeping, herb picking, refinement
* Supplies and outfit management
* Astrolabe astrology information
* Rich and powerful queue management (waaaaaay beyond strategems) you can tap into (many of the above use it)
* for Achaea (unsupported): crowdmap functions, design catalog, and few of the above also work

# Packages and Features #

## CrystalNotices ##
* For Lusternia or Achaea.
* Adds the ability to create displays with multiple colors and over multiple lines
* Also includes some common functionality and functions shared by other packages (I should have named this CrystalCore)
* **Required by all other Crystal packages**
* Strictly internal use (no commands or syntax, so no help command)

## CrystalWaypoints ##
* For Lusternia or Achaea.
* Define your own waypoints (like landmarks, but just for you)
* Walk to them easily
* Automatically make clickable links out of `SCENT`, `WHO`, `EWHO`, and people naming rooms in format v12345
* `FINDROOM <text>` to find rooms anywhere that have that text in the title
* `SCENTTO <person> [command]`: scent a person, walk to them, and optionally do a command on arrival (`p5` is shorthand for doing a perfect fifth; use `$` for the person's name) - only applicable in Lusternia
* CrystalQueues is required for `SCENTTO` in Lusternia
* If used alongside CrystalCrowdmap, links will use CrystalCrowdmap for pathing and walking

`WAYPOINT HELP` to learn more.

## CrystalQueues ##
* For Lusternia or Achaea
* Primarily used internally to queue actions
* Easily queue repeating a particular balance-using action
* Or make your aliases, triggers, and keybinds queue actions with many features

`CQ HELP` to learn more. Also see **CQueuesInstructions.md** in this repository.

## CrystalShip ##
* For Lusternia only
* Supports siphon, turret, grid, and command chair.
* Works with the standard captain calls used by the well-known Mudlet packages (Windmills, Dreadnought, etc.).
* All the features of those Mudlet packages, plus:
* When using the grid, target by module name or number, or the person on the module.
* When doing a slivven check, identifies room names and numbers for known ships.
* Automatic aethertrading that gets the highest return possible at the trader.
* Command chair autopilot following the algorithm determined by Tristanna.
* Turret includes a special mode for non-finks on ships with finks for higher efficiency.
* Tracks aetherwill and announces when it's running low.
* Slivven warnings.
* Can use scent to planarbond and covey all on the ship in a single command.
* Personality: customize the messages it shows, and that you say on ship channels.
* Easy power distribution by module name or number, or the person on the module.
* Gagging to keep the scroll manageable.
* Cycling siphon calls.
* Screen reader mode.

`CS HELP` to learn more. Also see **CShipInstructions.md** in this repository. __Note special install instructions there__!

## CrystalFactions ##
* For Lusternia only at this time
* Can look people up from the API or infer via HONORS to determine faction membership
* Cache faction membership for quick response
* Highlight names in who, channels, scent, aethersight, etc.
* Track ally and enemy lists, color based on enemy status
* Create, save, and restore ally and enemy lists for many situations

`FACTION HELP` to learn more. Also see **CFactionsInstructions.md** in this repository.

## CrystalHunt ##
* For Lusternia only
* Handles hunt attacks including beasts, before/after, panic, razing, collecting corpse/essence/gold.
* Automatically adjusts to lowmagic/highmagic.
* Hunt a particular target repeatedly, or a list of targets.
* Configurable actions for attack, raze, panic, before, and after.
* Save hunting lists by area or name.
* Handle hunting mobs that drop from mobs.

`CH HELP` to learn more. Also see **CHuntInstructions.md** in this repository.

## CrystalInfluence ##
* For Lusternia only
* Handles influencing of any type, adapting to your skills.
* Influence multiple targets in sequence until exhausted.
* Works for order realm creatures that vanish after influencing.
* Can do storytells before, if configured to.
* Can use beasts for ego heals.
* Configurable actions for before and after targets, groups, and the whole influence.
* Automatic imbuing with configurable target and before/after commands.
* Automatic maintenance of a song for bards if configured.

`CI HELP` to learn more. Also see **CInfluenceInstructions.md** in this repository.

## CrystalDebate ##
* For Lusternia only
* Automatically handle debating.
* Attempt to determine patterns in opponent actions and counter them.
* Support focus pendants, fasttalks, and attitudes.
* If available and enabled, use roulade for backup healing.
* If enabled, run away if losing, and chase if your opponent runs (requires artifact ears).

`DEB HELP` to learn more. Also see **CDebateInstructions.md** in this repository.

## CrystalSupplies ##
* For Lusternia only
* Define low and high restock points for every rift, liquid rift, and other consumable
* Check to see if anything needs to be restocked
* Get a report showing both necessary and optional restock levels

`SUPPLIES HELP` to learn more. Also see **CSuppliesInstructions.md** in this repository.

## CrystalOutfits ##
* For Lusternia only
* Create as many outfits as you want
* Wear them with or without a glamrock
* Handles item poses, earring locations, and more

`OUTFIT HELP` to learn more. Also see **COutfitsInstructions.md** in this repository.

## CrystalShop ##
* For Lusternia only
* Manage multiple shops
* Track inventory against desired stock levels and get restock report
* Automatically stock most items

`SHOP HELP` to learn more. Also see **CShopInstructions.md** in this repository.

## CrystalAstrolabe ##
* For Lusternia only
* Display what an astrolabe would, plus additional information, from anywhere
* Specify a date and optionally hour to get a reading for another time
* Also automatically does a reading when you do someone's nativity
* Usage: `ASTROLABE [yyy-mm-dd-hh]`

## CrystalHerbs ##
* For Lusternia only
* Automatically harvest one or more herbs.
* Follow a path to harvest in many rooms.
* Set a target, and how much it varies by month.

`CHARVEST HELP` to learn more. Also see **CHerbsInstructions.md** in this repository.

## CrystalRefine ##
* For Lusternia only
* Automate the process of refining for aethertrading.
* Supports the use of Psychometabolism Bodyfuel for power regeneration to quicken the process.
* Requires CrystalNotices and CrystalQueues to be installed.
* Does not currently support refining when you might be off balance (e.g., while hunting/fighting).

`CR HELP` to learn more.

## CrystalDefenses ##
* For Lusternia only
* **Note!** __This package is far from complete and kind of clunky!__ I'm sharing what I have in case others want to take it from here. It has only some skillsets, some of those probably badly outdatedly so; not all wonder items or gear; etc. and it's not nearly configurable enough.
* Define profiles as groups of defenses and raise/lower things to switch between them.
* Profiles can tie defenses to skillsets and then automatically ignore them if you don't have that skillset active, so one profile can serve for the same purpose in many skills or even classes.
* Pseudodefenses for certain kinds of gear, so you can, for instance, put on your circlet when you prep for astral.

`DEFS HELP` to learn more. Also see **CDefensesInstructions.md** in this repository.

## CrystalCrowdmap ##
* For Achaea only (Lusternia does all this natively)
* Uses the crowdmap to display information about hidden exits as you enter a room
* Quick optimized pathing, generally slightly faster than built in PATH and WALK commands
* Pathing can use wormholes, the Gare, and the portal at Sea Lion Cove and Siroccian Fortress, if configured to
* System can automatically reroute on encountering problems so you still get where you're going
* Integrates with CrystalWaypoints to route to locations by name
* Requires CrystalNotices and CrystalQueues, and integrates with CrystalWaypoints if it has it

`CCM HELP` to learn more. Also see **CCrowdmapInstructions.md** in this repository.

## CrystalDesigns ##
* For Achaea only (Lusternia does all this natively)
* Capture all designs in one or more trades
* Do a rich search of all fields on all designs
* See the design with your search terms highlighted
* Right now tailoring designs are also stored in the cloud so no need to capture them
* Other trades can be added as well if people send freesia@foobox.com the design.json file from `CDESIGNS DUMP`

`CDESIGNS HELP` to learn more.

# Installation #

## Downloading ##

Go to the [Downloads](https://bitbucket.org/HawthornThistleberry/crystalship/downloads) page (see the leftmost bar) and download the zip file, then unarchive it. The .nxs files in the Packages folder are the packages; everything else is documentation.

You can also browse the folder lists above. Documentation may be easier to read here, in fact, and packages can be individually downloaded by right-clicking them and saving.

## Installation ##

First, get to a safe place. Installing and configuring may take a few minutes and you don't want to be interrupted by a cave-fisher while it's happening.

All of these packages require CrystalNotices, so drag that file into the Reflex Packages of your Nexus Settings to install it. Additionally, CrystalCrowdmap, CrystalFactions, CrystalHunt, CrystalInfluence, CrystalDebate, CrystalHerbs, CrystalRefine, and CrystalDefenses require CrystalQueues, so install that next, if you mean to use any of those packages. Then install any other packages you want. Note that you can use some packages and not others, apart from these dependencies.

Once you've installed all the packages you want, click Save Settings in Nexus Settings in the upper right. (Not necessary in Nexus 3.)

Now, for each package you installed, and starting with CrystalNotices and CrystalQueues first, type `cinstall <package>`. The package name can be the name with or without Crystal, e.g., `cinstall crystalship` or `cinstall ship` both work. Case doesn't matter. What does this do? It simply does some basic variable initialization and any other default setup necessary. Nexus ought to support this directly, but it doesn't, sadly. Note that this will never erase any settings you have set up from previous installations. **If you skip this step, expect to get all sorts of bizarre error messages!** Parts of the package will be in place trying to react to things while other parts haven't even been initialized!

After installing a package, *please review the documentation*. The top may include additional 'before you use this package' setup steps, and the document will also tell you how the package works and how to use it.

## Upgrading ##

You can check all packages for updates with `CUPDATES` or check any individual one with its help command. They will also all check ten seconds after you log in (so it won't get mixed up with everything else happening at login).

Uninstall the package from the Reflex Packages tab, then install the new version as described above (including the `cinstall` command). _Any settings or data from previous versions will be preserved._ If you are upgrading multiple packages, upgrade CrystalNotices first, then CrystalQueues, then any others.

# Nexus 2 or 3 #

These packages are all compatible with Nexus 3. They should also work in Nexus 2, but in case they don't, the last versions before Nexus3 came out are in the Nexus2 folder. I will not be supporting these older versions, or Nexus2 in general, anymore, though.

If you already have Crystal packages installed in Nexus 2 and you're moving to Nexus 3, be sure to read [Nexus3.md](https://bitbucket.org/HawthornThistleberry/crystalship/src/master/Nexus3.md) for instructions in making the transitions. *Without following these steps, your Nexus3 install may freeze and refuse to allow input, and recovering from this will be much harder.*

Note that there are currently some limitations in the mobile and desktop clients that mean these packages will not be usable there -- they will probably do all the things but fail to display almost anything about what they're doing. They work fine from your browser on any device, though.

# Bug Reports and Suggestions #

See the [Issues](https://bitbucket.org/HawthornThistleberry/crystalship/issues) link in the left bar; you can see current issues and plans, vote for what I should focus on, add comments, even create your own bug reports. Or talk to Lendren in Lusternia or in the Lusternia or Serenwilde Discord.

All these packages are free for anyone in Lusternia to use (regardless of affiliation -- yes, even if you're Lendren's enemy). No cost, though if someone feels like tossing me a wondercrystal or some credits to express their appreciation I won't say no! But even just a sincere 'thank you' means more than you imagine, and useful input on the packages too.

# License #

These packages are free to use for anyone. Just don't claim it as your own work. If you want to make improvements, coordinate with me so I can keep them and my own work integrated. If you want to make your own package (for Nexus, Mudlet, or something else) based on anything here, feel free, but note in your readme, help, or docs some credit for my contribution to your work.
