# Purpose #

This package is for Nexus (the Iron Realms HTML5 web client) to provide a means of managing unlimited outfits, switch between them, and record them into a glamrock's couture slots. You can define exactly which bits of your outfit show in a couture and which are hidden, wholly independent of what you're actually wearing.

# Setting Up #

There's a lot of things to do before you can start switching between outfits, unfortunately, due to how complicated Lusternia makes things like wearing and removing clothes, or knowing what you're wearing:

* Specify where your unworn clothes are stored.
* Make a list of items you wear, that shouldn't show, but aren't listed.
* Define outfits.
* Go to where that storage location was when you change outfits.

Each of these will be described in a section of this document.

## Storage ##

Up until September 2023, cabinets, dressers, and armoires held an extraordinary, perhaps unlimited, number of wearable items each. Apparently that was a bug and they fixed it. As of this writing, some cabinets, dressers, and armoires hold 60 items, while others only hold 20, and there's no rhyme or reason, and no response to my bug report after a month or so. Hopefully it'll all get set to 60.

Because of the limit, and because Lendren has a really silly amount of clothing, I have made CrystalOutfits, as of v2.1.0 in October 2023, able to handle up to three separate storage units, but they all must be located in the same room. (Three dressers takes up 135% of a room's normal furnishing space, so with enough artisan skill and boosts, you can get to three, but I don't think anyone can get to four.)

The only way I could find to make this work is to divide things up in a way that seemed to about evenly distribute everything for me. Specifically, this is the way I divvied things up into three:

1: boxers,bra,chemise,panties,bonnet,cloak,coat,gloves,hat,scarf,shawl,dress,gown,longskirt,sari,shortskirt
2: belt,kilt,shirt,suit,trousers,tunic,boots,sandals,shoes,socks,robes,scabbard
3: anklet,bracelet,brooch,collar,cufflinks,earring,hairpin,necklace,pendant,pin,ring

But fret not. If your closet is much more manageable than mine, you don't need all three. How CrystalOutfits will work:

* If you have only one, it'll expect to find everything there, and put it back there too.
* If you have two, it'll use the first one for items in #1 above, and the other for all the rest.
* If you have three, it'll use them as above.
* Anything that's not in the above list will end up in #1.

To specify what your storage units are, `OUTFIT STORAGE <container>` where you can list up to three containers. For instance, `OUTFIT STORAGE ARMOIRE` if you only have the one, or `OUTFIT STORAGE DRESSERA123 DRESSERA124 DRESSERA125` if you have three dressers.

## Hidden Items ##

Take a look at `ITEMLIST CLOTHING` and `ITEMLIST JEWELRY`. Everything on those lists, CrystalOutfits will be able to correctly and automatically handle removing and removing. That is, if one of those items is in the outfit you define, it will appear in the resulting ensemble, and if it's not, it will not. Spiffy, right?

Not so spiffy. The problem is, you're probably wearing at least one, and maybe as many as dozens, of items that appear in your description but do not show up on either list, nor on any other list that CrystalOutfits can reliably extract. Artifacts are notorious for this -- I have multiple spectacles, boots, goggles, body parts (ears, nose, wings, etc.), pseudo-clothes (tam, gloves, masks, mantles, etc.), psuedo-jewelry (rings, amulets, etc.), and more, that should be worn but should not show up. There are even non-artifact items I want to be wearing but don't want to be part of my outfit, which don't show on either list anyway; for instance, my watch.

Lusternia has built methods to hide items in your outfits. Those might already do what you need. I'm not sure how well they work, particularly when interacting with glamrocks, or if you remove and replace them, so CrystalOutfits does not depend on this method. It, instead, removes everything that's not in the outfit, so it definitely won't show. (If you're recording to a glamrock, it'll restore it all after; more on this later.) But this means you need to tell it what items it won't be able to find on `ITEMLIST CLOTHING` and `ITEMLIST JEWELRY` that should not appear in your outfits (unless explicitly added).

To do this, `OUTFIT HIDE <item>` to build up a list. You might just look through your inventory to identify this, or after you've tried out wearing an outfit, if something shouldn't be there, add it to the hidden list and try again. `OUTFIT REVEAL <item>` to remove it from the list, and `OUTFIT HIDDEN` to see the list. Just remember to add your new artifacts to this list when you get them, if you wear them and don't want them to show.

## Always Worn Items ##

CrystalOutfits does its best to determine what you were wearing before you started, and restore it after it's done its magic (if you are using couture at least). But it can only do so much because there's no usable `ITEMLIST WORN` that shows everything with IDs in a capturable format. As noted above, it captures what it can and you can force things not on the lists it has to be hidden. But what about the opposite problem: a thing that you want to be really wearing and also appearing to be wearing, but which isn't jewelry or clothes?

For example, I am always wearing my Maylean bindi, my spectacles, my symbol of Maylea, and my Flame of dae'Seren, and they're part of my 'base' outfit so I'm also always appearing to be wearing them. CrystalOutfits always puts them on when I run an outfit and store it in couture, but since it doesn't know they were worn beforehand, it also takes them off and puts them in my dresser afterwards, which means I can't hear messages from Maylea, or jump around Serenwilde, etc.

To remedy this, I add these items to my 'always worn' list. You only need to do this if you use a glamrock, if you have items you want to always be wearing, _and_ those items are included within an outfit. But there's no harm in adding items to this list even if you don't always meet any of these conditions. Anything on the list will simply always be put on at the end of any couture recording process.

`OUTFIT WORN ADD <item>` adds an item to the list. Remove items with `OUTFIT WORN REMOVE <item>`, and see the list with `OUTFIT WORN LIST`.

## Define Outfits ##

In CrystalOutfits, an outfit consists of a name, then a list of items that will appear, optionally with a pose on each. Every outfit is independent; that means the same item can appear with different poses in different outfits.

Normally, the names of outfits don't matter, you can name things anything you want. However, there is one special outfit name, `base`. This outfit is used to store items that you want to wear in every outfit. Things like your wedding ring, your holy symbols, etc. If you're not using a glamrock, this is also where you'd put jewelry and artifacts that you want to have on all the time, to benefit from their powers. Whatever outfit you wear, everything in `base` also gets worn first. By putting these items into `base` not only don't you have to put them into every other outfit, you also can change `base` any time your basic loadout changes, without every other outfit needing an update.

To make an outfit:
* `OUTFIT MAKE <name>` or `OUTFIT COPY <from> <to>`
* For each item to add to it, `OUTFIT ADD <outfit> <item> [pose]`

Items should generally be specified by name and number, e.g., `dress23423`. You can use just the number, but later, when you are looking at the outfit with `OUTFIT SHOW <outfit>`, you won't know what 23423 is, and CrystalOutfits won't either. At least `dress23423` gives you a good hint. But whatever you specify has to be an actual noun that can be used to get, wear, remove, and put the item; you can't put `bluedress23423` because you can't `GET` it that way.

Some items can't just be worn but have to be worn somewhere specific, notably, earrings and studs. CrystalOutfits will try to put the first one of these it finds in the left ear and the second one in the right ear, but that's as far as it will try to be smart. You should specify any item that needs to go into a specific place by identifying the item with this special syntax: `earring453533=left_nipple`. You need to use the equals sign, use underscores in place of spaces in the location, and have no spaces in it, for this to work. Also note that items like this should not have poses.

Any other item can, optionally, have a pose. This will appear after the item when people look at you.

Here's an example. These commands:

* `OUTFIT MAKE SKYCLAD`
* `OUTFIT ADD SKYCLAD bindi12345 in the center of the forehead`
* `OUTFIT ADD SKYCLAD symbol68790 pinned to the chest`
* `OUTFIT ADD SKYCLAD spectacles432352`
* `OUTFIT ADD SKYCLAD earring453532=left_nipple`
* `OUTFIT ADD SKYCLAD earring453533=right_nipple`
* `OUTFIT ADD SKYCLAD ring21577`

would result in an outfit that looked like this on `OUTFIT SHOW SKYCLAD`:
```
Your outfit named skyclad includes the following:
 - bindi12345          (in the center of the forehead)
 - symbol68790         (pinned to the chest)
 - spectacles432352
 - earring453532=left_nipple
 - earring453533=right_nipple
 - ring21577
```

and ultimately in this when someone looked at you:
```
She is wearing:
a twining wedding band of vines and rainbows
a translucent crystal bindi in the center of the forehead
a blooming symbol of Serenity pinned to the chest
a pair of iridescent round spectacles
a simple silver hoop through her left nipple
a simple silver hoop through her right nipple.
```

Note that the order of items in the outfit does correspond to the order of the commands used to wear and remove them, but that does not seem to reliably determine the order of the items in the view. Lusternia's ways are mysterious and baffling. Don't waste the time removing items and re-adding them to tweak the order. Or if you do and it works, tell me the secret.

# Wearing Outfits #

At last, you're ready to get fashionable! There are two ways to wear outfits: with glamrock, and without.

## Glamrock ##

I'll be honest here. The main purpose I had in mind as I wrote this was for using it with a glamrock because I have had a glamrock since they came out (in fact I got the first one). In this mode, when you tell CrystalOutfits to do its thing with a command like `OUTFIT WEAR bluedress 3`, what it will do is this:

* Make a list of everything you're wearing by combining your `OUTFIT HIDDEN` with what can be seen on `ITEMLIST CLOTHING` and `ITEMLIST JEWELRY`
* Take all that off
* Put on everything in the outfit (getting it from your storage as needed)
* Record this as a couture in the slot you specify
* Take it all off and put it away
* Put back on everything you were wearing before
* Play the couture it recorded

The end result is that your screen spams with scroll for about 30 seconds (some of it gets delayed to avoid the MUD punishing you for too many commands at once), and then at the end, if everything was set up right, you are *wearing exactly what you were wearing before* in your inventory, and for all practical purposes like armor and magic items, but you *appear to be wearing only what's in the outfit*. That's what it's all about.

So ultimately the real purpose of CrystalOutfits is being able to have more than 10 outfits, even if you have only 10 glamrock slots.

Remember all the glamrock rules still apply. Any item that decays makes the whole outfit go away. Putting it all back into your cabinet afterwards helps prevent that ever happening.

Another clever trick: if you get an item that doesn't decay, but does reset (e.g., the flowers that come from Lady Maylea's willow), you can add them to an outfit, then record it. Since the flower still exists long after it's left your hands (because it went back to the willow), it still appears in your couture. But you can't rerecord without losing it!

## Actually Wearing ##

If you don't have a glamrock, or for any reason you want to actually physically be wearing stuff in your outfit, just don't specify a slot, e.g., `OUTFIT WEAR BLUEDRESS`. The process here will be similar, but shorter:

* Make a list of everything you're wearing by combining your `OUTFIT HIDDEN` with what can be seen on `ITEMLIST CLOTHING` and `ITEMLIST JEWELRY`
* Take all that off
* Put on everything in the outfit (getting it from your storage as needed)
* And that's it!

Now you're actually wearing that outfit. Which means anything that's not in the outfit, but needs to be worn to be used, is not usable. This is where you might want to use server-side hiding, and put those items into the `base` outfit.

This mode has not been tested very much. Please report problems, ideally with logs. That log is going to be very long and spammy, and it's going to be nigh useless if you haven't turned on command echos in your Nexus settings.

Note that in this mode, CrystalOutfits has no way to automatically return you to what you were wearing beforehand. Maybe you should have recorded that as an outfit too!

# Problems and Suggestions #

Please send your thoughts to Lendren. Can't make it work better for you if you don't tell me what could work better!

