# Purpose #

CrystalQueues is not something that the player will spend a lot of time interacting with. Its primary purpose is to be an engine below the surface that takes care of queueing actions and then executing them. It has similar functionality in principle to the Combat skill Strategems, but a great deal more functionality. (But since strategems are server-side, they can be faster due to ping latency, by a factor of milliseconds.)

What's a queue? In this context, it's a list of commands that need to be done, when the time is right to do them. Basically, things that we can't do right now, but we can do them later, and should do them once we can. Why can't we do them now? Here are the main sets of reasons:

* Balance: they require balance, equilibrium, psi balances, dust balances, etc.
* Health: they require health, mana, ego, or power above or below a particular value.
* Semaphores: one queued command can't execute until another one has executed.
* Blockers: something is happening that means you shouldn't or can't do anything until it's done.

As a simple example, suppose you want to raise ten defenses, some of which consume balance. With CrystalQueues you can queue them all up, and then sit back and let CrystalQueues figure out which ones can be run when, and get them all done at the quickest possible pace. This even works when some of them require power, or can only be run when at full mana (like Hod), or depend on one another (like Sycophant depending on Performance). And in fact, that's exactly what CrystalDefenses does.

# Commands #

The meat and potatoes of CrystalQueues are functions that are not for the player to use, but for other scripts to use. More about how to have your own scripts use them below. Note that, if you get in the habit of making as many actions as possible go through CrystalQueues instead of having your aliases and triggers do them immediately, the better off you will be for having things that happen simultaneously not interfere with one another, or interrupt actions like Bardoon, or fail because you're stunned.

## Status Commands ##

Usually you can just let the queues run and take care of themselves, but occasionally you might need to intervene to view, pause, or clear the queue. Here are a few commands to know:

* `CQ STATUS`: Shows if the queue is running, paused, or blocked, and summarizes its status.
* `CQ SHOW`: Shows what's in the queue. Priorities are color-coded, reds for high, through the rainbow to purples for low.
* `CQ PAUSE` and `CQ RESUME`: Pause and resume the queue.
* `CQ RESTART`: If the queue says it's blocked, e.g., because it thinks you're Bardooning, this forces it to resume.
* `CQ WIPE`: Clears the queue.

In addition, if you create a Custom Tab named CrystalQueues, it will automatically be populated with a realtime view of your queues. Hover over any queued command to see more details (require, consume, and semaphores).

![CrystalQueues custom tab](https://cdn.discordapp.com/attachments/1104944381611491338/1131727098806022174/image.png)

## Notification ##

By default, CrystalQueues runs silently. But in case you'd like some information on what's happening, you can ask it to notify you. In this case it tells you when things are added to, removed from, or executed by the queues. These are your options:

* `CQ NOTIFY SILENT`: Be quiet, I'm trying to do something here!
* `CQ NOTIFY BRIEF`: One-liners that tell what is being queued or removed, but no details.
* `CQ NOTIFY VERBOSE`: As `BRIEF` but also shows commands and balances.
* `CQ NOTIFY EXCLUDE <label>`: If there's one action that happens a lot and you don't want to hear about it, and it has a unique label (it should!), add it to an exclude list and notification will never bug you about it even if it's on.
* `CQ NOTIFY INCLUDE <label>`: Reverse the above.
* `CQ NOTIFY EXCLUDED`: See a list of what you've excluded from notification.

## Quick Queue Commands ##

There are a lot of options for queueing a command, too many for any command line syntax. And that's okay because commands that want to use all those features are run by other scripts, aliases, and triggers (see below). But there are a few commands that help you do some quick-and-dirty queueing that can be very helpful for utility actions like crafting:

* `CQ MULTI <num> <cmd>`: Use this to queue up an action that requires and consumes either balance or equilibrium, that you want to run multiple times. For example, want to make ten palettes? `CQ MULTI 10 PREPARE PALETTE`. There's no reason you can't put in a 1 for things that only happen once, too. While those ten palettes are being made, `CQ MULTI 1 PUT 10 PALETTE IN PURSE` so once you've finished making them, you can put them away, too.
* `CQ NOBAL <num> <cmd>`: As above, but for any action that requires, but does not consume, balance. This is mostly helpful for slipping something into the stream while you're doing something. Influencing but you want to quickly check who's around with stopping the influence? `CQ NOBAL 1 SCENT` and the next time you get balance, you'll scent, then continue with influencing.
* `CQ ADD <label> <baltype> <cmd>`: This lets you sneak in a single command, provided it both requires and consumes a single balance type. This comes up more rarely than you'd think, which is why `CQ MULTI` and `CQ NOBAL` are the ones you're more likely to use.
* `CQ REMOVE <label>`: Most queue items have a label, which is the first column in `CQ SHOW`. For instance, anything made with `CQ MULTI` has `multi` for its label. This lets you remove a single item based on that label; if there's more than one with that label, it takes the first one.

# Queueing From Your Own Scripts #

When you want to put things into the queues from your own scripts (and you should!) this is how.

## Queue Elements ##

Each entry in the queue has the following elements. Understanding these is the key to using CrystalQueues effectively.

* **Label**: Just a name that the queue entry hangs off of, that you will use later to refer to it. It's good to make these short, unique, and descriptive. For instance, if you're queueing a command to do a Serpent, name it `serpent`. Queue entries can have no label, but if so, you won't be able to remove or change them.
* **Balances Required** and **Balances Consumed**: It is common that most commands will require a set of balances, like balance and equilibrium, and will consume some of the same balances but not all. For instance, most physical actions will require both balance (`bal`) and equilibrium (`eq`), but only consume one of them. CrystalQueues needs to know both so it can correctly determine what can be run when. See the next section to learn about balances, including a few things that aren't exactly balances but still requirements.
* **Priority**: From 0 to 9, where low numbers are low priorities. Yes, that's right, low numbers are low priorities, because that makes sense to me. CrystalQueues will always do higher priority commands before lower priority ones; however, if you have a high priority command it can't do because of requirements not met, but a lower priority one it can, it will still do that one.
* **Repeats**: Every queue entry can have a repeat count. 1 means to do it once and then take it out of the queue. Any higher number will count down every time it executes. -1 means to repeat forever, so it had better have a label so you can eventually have something else remove it; this is good for things like influence, where you don't know when you should stop, so you queue it with an infinite repeat, then the trigger when the influence is done removes the queue entry.
* **Command**: This can also be an alias.
* **Semaphore** and **Semaphore Requirement**: This is a simple concept but tricky to grasp. If you want one command to only fire after another, you give the one that fires first a semaphore, and then make the other one have a semaphore requirement. A command can have more than one of each (separated by spaces). A typical example: queue the Performance defense (`perform on`) with the semaphore `performance`, then queue the Sycophant defense (`perform sycophant`) with a semaphore requirement of `performance`. Now, as long as the Performance queue entry is still in the queue, that is, it hasn't yet run, the Sycophant queue entry will refuse to run. Commands can depend on multiple semaphores, or have multiple semaphores, or both, and a command can depend on another command that in turn depends on another command, so this can be tricky to follow if you start adding a lot of semaphores. It's even possible to build in your own deadlock (e.g., entry A depends on entry B, and entry B depends on entry A), but it rarely gets that complicated.

All these elements show in `CQ SHOW` though some might get cut off in that display. In the custom tab, they also show, but some of them are only visible on the hover-over on each command, to keep things narrow enough to fit in a small tab.

## Balance Types ##

The two main balances, `bal` and `eq`, are by far the most common, but CrystalQueues tracks and knows about a lot of others. Here's the complete list:

* `bal`: Balance. In the dawn of Achaean time this was the only one, which is why the rest are called balances.
* `eq`: Equilibrium, usually used by more mental skills.
* `larm`, `rarm`, `lleg`, `rleg`, `head`: Body part balances for monks and warriors.
* `psiid`, `psisuper`, `psisub`: Psionic balances.
* `slush`, `ice`, `steam`, `dust`, `heal`, `allheale`, `potion`: Balances for various cures (`heal` refers to all elixirs). Not nearly as important now as they were in the days before Autocuring.
* `beast`: Beast balance. A low priority queue entry that requires and consumes this with repeat -1 is a good way to keep your beast doing something in the background, like healing you, or attacking your target. Then you can put in a higher priority queue entry with repeat 1 to slip a special action in, like blocking or doing a specific attack.
* `module`: Aethership module balance for whatever you're locked into.

When something requires multiple balances, just separate them by spaces.

There are also four special psuedo-balances you can use to put requirements onto a command based on your vital stats. This is easiest explained with examples:

* `p>=3`: Requires three power. Will wait to execute until that power is present.
* `m>=max`: Only run when mana is at or above max. (Why not just `=`? Some skills push mana above max.)
* `h<300`: Run this if your health drops below 300.

These only go into the balances required, not consumed. They can be `h` for health, `m` for mana, `e` for ego, and `p` for power, followed by any comparison (`=`, `<`, `>`, `<=`, or `>=`), followed by either a value or `max`.

## Adding Queue Items ##

Here's a representative example of what it looks like in another script to add an item to the queue:

```
run_function("add_to_queue",["bal eq p>=2", "eq", "4", "1", "flare", "weave flare " + sCombatTarget,"",""],"CrystalQueues");
```

The arguments array is filled out as follows:

* `args[0]`: a string of balances required. In this case, only do this when we have balance and equilibrium and at least two power.
* `args[1]`: balances consumed; this can be 'same' if it's the same as the balances required, but it rarely is
* `args[2]`: priority (0-9, higher is more urgent)
* `args[3]`: repeat count (-1 means forever)
* `args[4]`: label for display in the queue lists
* `args[5]`: the command to execute; in this case, we're flaring the current target
* `args[6]`: semaphore: see description above
* `args[7]`: semaphore requirement -- don't run if any other action has this semaphore

Here are some other examples.

On the trigger `You feel that your esteem has reached its peak.`:

```
run_function("add_to_queue",["bal eq", "", "7", "1", "imbue", "imbue brazier with 500 esteem","",""],"CrystalQueues");
```

Balance and equilibrium are required but not consumed here. High priority to make sure it comes before the next influencing, but not so high that life-threatening things don't come first.

On the trigger `A glass alembic releases a sweet smelling cloud of white dust, as the ingredients crystallize into sugar.`

```
run_function("add_to_queue",["bal eq", "eq", "4", "1", "sugar", "get 10 sugar from alembic","sugar",""],"CrystalQueues");
run_function("add_to_queue",["bal eq", "", "4", "1", "inr", "inr everything","","sugar"],"CrystalQueues");
```

Here we see the use of a semaphore: you don't want to put the sugar away unless you've gotten the sugar out of the alembic. Without the semaphore, the inr would otherwise fire first since it doesn't require equilibrium, and you're off equilibrium at the moment you notice the sugar.

## Other Functions ##

To remove something previously added, assuming it had a label (it should always have a label):

```
run_function("remove_from_queue","flare","CrystalQueues");
```

For instance, when your target dies, you should probably clear queued attacks against them, including a repeating beast attack.

To check if a queue entry exists:

```
run_function("is_in_queue","flare","CrystalQueues");
```

Useful to avoid doubling up queue entries. Yes, if you send the same command with the exact same configuration through twice, it will create two queue entries, because sometimes you need that. Since Nexus functions don't have true return values, this will store a "" if it's not in the queue, or the command if it is, in the value you get with `get_variable('p0')` (which I use extensively as one of a few scratch variables for passing things between functions and scripts just like this).

# Limitations #

* CrystalQueues uses both GMCP and various triggers to try to keep track of balances. Due to the delay between sending a command and seeing the results, there are some cases where it can get briefly out of sync. (That's why you have to tell it about the balances consumed. It will *assume* those balances are gone, because it can't afford to wait to see them disappear while it's evaluating what else it can do.)
* As of this writing it only knows about three blockers, and it may not know all the triggers for them starting and stopping. It automatically stops doing things when it thinks you're stunned, when it thinks you're dead, and when it thinks you're trying to do a Bardoon. Obviously, if it thinks that, but you're not anymore (because I'm missing some trigger or message meaning that condition is over), it's going to get stuck. In this case, use `CQ RESTART` to get it going again, then send Lendren a log of what happened. Also, for people who do combat, it needs a lot more blockers, for other instakills or actions you don't want interrupted, for other conditions you shouldn't be acting during like Aeon (or only acting in limited ways), etc. Please share your triggers and information with Lendren to get these added!
* In combat, it can be hard to keep track of things. If you've queued up attacks or actions, by the time they come around the moment may have passed. Expect some complicated coding if you want to build up a system that knows when to both add *and remove* queue entries as situations change, and be sure to use really unique labels to make that possible.

