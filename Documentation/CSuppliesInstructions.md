# Purpose #

Ain't it a bummer when you're somewhere far in the deeps of some hunting area, or deep in a fight, when you discover that you're almost out of dust? Or when you step up to the alembic and find you're low on weed? What about after you're done at the alembic and need to know what to restock? Well, CrystalSupplies is here to help. You'll use it like this:

* First, set up a list of supplies to monitor, and low and high stock levels.
* Then you can check your supplies and get told what needs to be restocked.
* You can also get a report of everything that could be topped off.

## What CrystalSupplies Can Count ##

Here are the things that CrystalSupplies knows how to count:

* Everything in your rift.
* Everything in your liquidrift. Note that since `salt` and `sulfur` appear in both rift and liquidrift but are different things, CrystalSupplies will call the liquid versions `lsalt` and `lsulfur`.
* Your cube's charge level, which is called `recharges`.
* Hunt and influence origami, that is, `kirigami` and `wetfold`.

## Setting Up ##

Tell CrystalSupplies you want it to count something by defining its low and high stock levels, as follows: `SUPPLIES NEED <item> <low> <high>`. For instance, `SUPPLIES NEED wetfold 5 10`.

What are low and high stock levels? Here's what they mean:

* If you have the high level or more, you're fine. This won't even show on reports.
* If you are above low, but below high, this is an optional restock, and only shows on the full report.
* If you are below the low level, you need to restock now.

In the case of the wetfold here, I don't want to restock, then the minute I use one, be nagged to restock again. So I build my stock up to ten, then I have a little while before it's dropped to below five, and at that point I start to get nagged. Then I restock all the way back to ten.

To remove something you previously said to track, `SUPPLIES UNNEED <item>`. Maybe you no longer need some supply because you changed class and no longer have a skill that uses it.

`SUPPLIES LIST` shows a table of your needed supplies. Before you've done a `SUPPLIES CHECK` (see next section), the "have" column will be all zeroes, so everything will be flagged in red as needing restock, but once you've done an inventory, this will show actual values and use colors to show the same thing that reports show.

`SUPPLIES SHOW <item>` shows the detail of a single supply.

One last thing: you probably don't keep your origami sitting out in your hands, but keep it in a container. (In fact, to track them, CrystalSupplies requires you to do so.) Use the command `SUPPLIES CONTAINER <item>` to tell it what container to peek into to find those. For instance, I keep mine in my purse.

## Checking Supplies ##

CrystalSupplies does not try to monitor your supplies on an ongoing basis at this time. Instead, you periodically check it. (Maybe I'll add some real-time monitoring one day.) Simply type `SUPPLIES CHECK` to have it do an exhaustive inventory of your rift, liquid rift, cubes (in your hands), and origami (in the container you specified). At this point it will automatically give you a basic report (see below).

If you want to do this every time you log in (highly recommended), I suggest you add this to the `onLoad` function in your main package:

```
setTimeout(function(){ send_command('supplies check',0); }, 12000);
```

The 12000 here means to do it 12 seconds after you've logged in. This is to give time for all those spammy commune/guild/order messages to have passed, and the other things you're doing on logging in. 

## Basic Report ##

`SUPPLIES CHECK` automatically runs `SUPPLIES REPORT` for you, which only tells you about things below your **low** stock levels, that is, things that need to be stocked right now. If nothing is, you'll just be told nothing needs to be restocked. Otherwise you'll see something like this:

```
Supply           Have   Low  High   Min  Full
--------------- ----- ----- ----- ----- -----
kafe               20   300  2000   280  1980
bluetint            0    10   200    10   200
--------------- ----- ----- ----- ----- -----
```

(The actual display will be color-coded.) Here you can see that, for kafe, we need to add 280 just to reach the minimum stock level, and 1980 to read the high stock level. Similarly, we're totally out of bluetint and need at least 10 to put out the fire, or 200 to be satisfied we won't need to think about bluetint for a while.

## Full Report ##

If you do a `SUPPLIES REPORT FULL` you will get a longer report that draws attention not only to the must-have stock items above but also things that are just not quite at full. It might look like this (only with colors to help you see what's more or less urgent):

```
Supply           Have   Low  High   Min  Full
--------------- ----- ----- ----- ----- -----
============= MUST BE RESTOCKED =============
kafe               20   300  2000   280  1980
bluetint            0    10   200    10   200
--------------- ----- ----- ----- ----- -----
============== CAN BE RESTOCKED =============
recharges        3987   100  4000          13
silvertint        192    10   200           8
gold              131    50   200          69
marble            861   100  1000         139
vegetables        149    50   200          51
vellum            180    20   200          20
wetfold             7     5    10           3
--------------- ----- ----- ----- ----- -----
```

In this example you can see I topped off my cube not long ago, could stand to do some shopping, and making a few more wetfolds might be a nice way to pass the time, but I really don't need to do those things, I'm good for a while. This report is particularly useful for that situation where you just made a bunch of potions, tints, or whatever, and now you want to replenish your herbs while you have time to kill.

Once you've restocked, just do a new `SUPPLIES CHECK` to update the counts CrystalSupplies uses.

