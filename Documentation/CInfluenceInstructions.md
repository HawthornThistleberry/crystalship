# Setup #

## Prerequisites ##

* CrystalNotices must be installed (the latest version).
* CrystalQueues must be installed and set up (see **CQueuesInstructions.md**).
* CrystalDefenses is not required, but CrystalInfluence will work with it if it's present.

## Installation ##

As with any Crystal package, once you have installed it (even if you're only installing an update), type `CINSTALL INFLUENCE` to force it to initialize variables and get itself set up. This won't change any configuration you already did, when upgrading, so don't worry.

## Skills ##

One thing this will do is check your skill lists for Influence and Dramatics. Also, any time you study either of those skills, CrystalInfluence will check the list again. This way, it automatically knows which of the Charity skills you have, so it can use only one, two, or all three. This also makes sure it knows what Storytell skills, in case you choose to use those.

If you ever have your skills change from something other than training (e.g., changing faction, or losing a divine favor) you can repeat `CINSTALL INFLUENCE` to get it back into synch, or it will do so the next time you log in anyway.

## Jumping Right In ##

Before you start using CrystalInfluence to influence, you need to configure a handful of things. There's a lot of power here, but that means there's a lot to explain in this document. But if you want to just jump in, do `CINSTALL INFLUENCE`, then skip down this document to the **Influencing** section. Then come back after you've done a little influencing to see all the configuration you can use to make it better.

## Defenses ##

CrystalInfluence does not, itself, manage any defenses, not even influence-related defenses like Sycophant, but it does integrate with CrystalDefenses to do so, if it's installed.

To make the best use of CrystalInfluence, simply make a set of defense profiles named Weaken, Charity, Empower, Paranoia, Seduction, Village, Amnesty, and Basics. You may also want to make profiles named Animalempower, Animalweaken, Undeadempower, and Undeadweaken, if you have abilities to influence animals and/or the undead and those abilities need (or could benefit from) a different defense profile.

If CrystalInfluence sees that you have CrystalDefenses installed, every time you set an influence type, CrystalInfluence will automatically switch you to that profile and raise any defenses needed. When you end influencing with `CI DONE` it switches you to the Basics profile the same way (and also automatically lowers CharismaticAura).

So, for instance, your Empower profile should include the usual things like Beauty, Wetfold, and (if you have it) CharismaticAura, plus Empower-specific things like Performance and Sycophant. Here's what mine looks like, by way of example:

```
Profile empower contains:
 - sycophant                      - dragonsblood                  
 - charismaticaura                - thirdeye                      
 - nightsight                     - music:bardicpresence          
 - adroitness                     - deathsight                    
 - beauty                         - perfection                    
 - performance                    - highmagic:netzach             
 - wetfold                        - mask                          
 - harmony:ooshmun                - harmony:ooshphet              
 - harmony:anguti                 - dramaturgy:etiquette          
 - wondercorn                     - pumpkinpie                    
It has 20 defenses.
```

The animal version might add `wondermaskdryad` to the above.

## Options ##

Some more options you should configure:

* `CI BEAST ON/OFF`: If you turn this on, CrystalInfluence will automatically ask your beast to try to an ego heal on you when on beast balance. Note that this means it's going to ask way more often than your beast has enough ego to heal you. No problem, if you ask for beast heals and the beast can't help, it gives a message but nothing bad happens. Better to just always ask. It might save some bromides. Of course you shouldn't turn this on if your beast doesn't have that power (or you don't have a beast).
* `CI IMBUE <item>`: Set the name of your item to imbue (e.g., `brazier`) and CrystalInfluence will automatically imbue when you get full, or do `CI DONE`. Set to `NONE` and it won't handle imbuing.
* `CI SONG <name>`: If you are a bard, and you want to have a song up when you influence, set the name of the song here. CrystalInfluence will assume you have played the song, it won't handle that, but it will refrain at the start of your influencing (unless you're already playing) and sustain it when you do `CI DONE`, plus it will refrain it when it's about to run out. If you use CrystalDefenses, it'll even know to skip doing that if you have skillflexed away from having Music, and resume it once you have Music again.
* `CI STORY ON/OFF`: The idea of storytell skills in Dramatics is that it makes the targets worth more experience, esteem, and essence by strengthening them, which is particularly good for targets where you only have so many you can influence and then have to wait for them to reset (like guards). If this is turned on, CrystalInfluence will do the appropriate storytell before the influence for each target. If you have an endless supply of influence targets, the time you spend on storytelling may or may not be worth the experience gain, but if you have an endless supply of influence targets, why are you even worrying about it?
* `CI STORY SKIP ADD/REMOVE <target>`: Even if you have Storytelling turned on, some targets you might not want to bother to tell a story to, because it won't help (e.g., order realm denizens). Add them to this list and they won't be bothered with any stories. `CI STORY SKIP LIST` to see who you've excluded. Note: if you are influencing animals, and you try to storytell to them, they will automatically be added to the list since you can't tell stories to animals.

## Before and After ##

The most powerful configuration available to you is the ability to have CrystalInfluence do certain commands before and after each of four events. Let's lead with an example:

`CI BEFORE INF NONE WEAR MASK`

What this command does is ensure that, at the beginning of any influence session, you wear your mask. Simple, but this can be very powerful. Let's look at the various events and then what you can set for each.

### Events ###

These are the events you can make things happen before and after:

* **Inf**: An entire session of influencing. That is, all the influencing that results from a single command (more on this later). Good for setup things (other than the ones that CrystalDefenses are also doing as noted above) like putting on a Mask of Esteemed Beauty, and taking it off after.
* **Target**: Happens for each individual target being influenced.
* **Imbue**: Happens before and after imbuing of esteem. Good for things like getting out your figurine and putting it away, or offering it with your font.
* **Done**: Happens at the beginning and ending of the actions of `CI DONE` (see below).

Each event can only do one command. So what if you want to do multiple things? Just create a Nexus alias that does all the things you want, and then set the command to do that alias. Maybe you'd just create an alias named `aftertarget` and then `CI AFTER TARGET BAL AFTERTARGET` and it'll do that alias after each target.

### Balances ###

For each command, you need to say what kind of balance it consumes, so that CrystalQueues can handle it properly. Your options are `BAL` for things that consume balance, `EQ` for equilibrium, and `NONE` for things that consume neither. (If you're doing the alias option, if everything in it together consumes a single balance, just use that. If you want multiple things that each have their own balances, use `NONE`, then set the alias up to do CrystalQueues calls or commands to queue the actions you want. See the CrystalQueues documentation.)

Special syntax: to clear a command, that is, make it so nothing happens, just do `CI BEFORE/AFTER <event> NONE` (without any command).

To see all your commands, plus a reminder of what the events are, check out `CI COMMANDS`.

Combining all this you can do some pretty powerful stuff. Tell me about what you came up with for it.

# Influencing #

## The Start Command ##

The simplest way to do influencing is the `START` command that does everything you need in a tidy package:

`CI START <type> <target list>`

For example, at the Ethereal Mother Tree:

`CI START EMPOWER HUNTER HAG WARDER`

What this does:

* Sets the influence type to Empower. That selects the appropriate influence commands to use, and storytell as well, if you have that activated.
* If CrystalDefenses is installed, it'll also switch you to the correspondingly named defense profile.
* Refrain your song, if you have one configured and it's currently sustained.
* Do all the before and after commands, if configured, as it goes through the remaining steps.
* Influence the first hunter.
* When that's done, keep doing hunters until they're all done. It knows this when it tries to influence one and the hunter is patronizing because he's already influenced.
* Moves on to do the same with hags, then with warders.
* Imbue every time you get to full esteem.

This is probably the command you'll use most often since it's everything in one nice package wrapped in a bow, but you might also sometimes use these other commands that do individual pieces of the above.

## Quick! ##

Use `CI QUICK <type> <targets>` to do the same as `START` except that all preparatory steps are skipped -- defenses, songs, and storytelling. This is good for things where you need to get dibs in on something quick (e.g., in a village, or a gnome) and especially when you don't need to fuss with all those preparations since it's an easy influence anyway.

## Setting The Type ##

Use `CI TYPE <type>` to set up the influence type in advance. This also raises the defense profile and refrains your song, but that's it. This is good for when you're about to do village influencing. That ensures you don't accidentally empower someone in a village, which is mortifyingly embarassing. (Not that I would know that. (Okay, so it was me.))

Animalempower and Undeadempower are treated exactly like Empower, except that if you use CrystalDefenses, they will raise a different defense profile. The same is true for Animalweaken and Undeadweaken. If you don't use CrystalDefenses there's no reason to use these.

## Single Influence ##

If you know you only have one target to influence, `CI INF <target>` does only one, and stops. That's a tiny bit faster, since it won't waste a balance trying to influence again. Good for named targets. If you note a target that's not present, it will spam four times trying to influence it and then stop (this prevents random 'not there' messages from unrelated things from interrupting your influences).

## Multiple Influence ##

Once the influence type is set, `CI MINF <target list>` is your meat-and-potatoes command. You can give it a single target or a list it'll work its way through. This even works on targets that vanish when influenced, notably order realm denizens. It also automatically adjusts its list of targets as denizens move in and out of your room (and always does them in the order you specified).

## Ending Early ##

If you need to stop a round of influencing, you can. You have two approaches:

* `CI STOP`: Finish the current target, then stop everything.
* `CI STOP NOW`: Stop immediately. It won't try to divert or anything. You're in control now. Maybe it's time to run for your life, or debate, or fight, or log off.

## Wrapping Up ##

When a round of influence finishes, maybe you just move on to the next room and continue. But what if you're really done? There's a command available for this, which you may or may not need. `CI DONE` tells CrystalInfluence that you don't intend to be doing any more influencing for a little while. What it does:

* Force all influencing to stop, instantly, like `CI STOP NOW`.
* Do any Before Done command you configured.
* Imbue, if you have set an imbue item. Also the before and after imbue commands.
* If you're using CrystalDefenses, switch you to the Basics profile.
* If you're using CrystalDefenses and have CharismaticAura up, lower it, to save the heavy mana drain.
* If you configured a song, sustains it. (If you have CrystalDefenses and you skillflexed out of Music it knows to skip this.)
* Do any After Done command you configured.

Some people will never use this command. Let me know if there's things you'd like it to do that are not on this list (and not handled by `CI AFTER INF` or CrystalDefenses).

## Setting Common Target Rotations ##

If you're like most influencers, there are a handful of target groups you do a lot. Your guards, your order denizens, those Jolly Bundy gnomes, maybe a few others. CrystalInfluence does *not* try to help you keep those lists at this point. But it's very easy to set up Nexus buttons or aliases to do them. Right now, my system includes four Nexus buttons that do frequently done influences:

* F7: Guards (hunter, hag, warder)
* F8: Mother (hunter, hag, warder, Adasser)
* F9: Dryads
* F10: Gnome

If you'd like something smarter than this, talk to me about it. (But note that GMCP is not able to give a usable list of "influenceable targets in this room" without huge lookup tables, due to a few minor and easily correctable flaws that go uncorrected, perhaps on purpose.)

## Villages ##

CrystalInfluence has built in a set of aliases for each of the villages. Just `CI MINF <vilage>` and CrystalInfluence will find everyone in the room that's on the built-in list of targets for that village, and influence them. This is useful both for general influencing (e.g., `CI START CHARITY ESTELBAR`) and village revolts (e.g., `CI TYPE VILLAGE` and then `CI MINF PTOMA`).