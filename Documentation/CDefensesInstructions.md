# This Is Incomplete! #

* It has only a smattering of skillsets, a handful of wonderitems, etc. It's missing so much.
* Probably a lot of what it has is out of date. I haven't kept up with even the skillsets I have access to, let alone those that I no longer do, or never did.

## What's improved since v0.0.1? ##

* Needs an update to CrystalNotices so install that one first.
* A rewrite of the core engine to a much more scaleable and extensible approach.
* The configuration of the defenses, apart from the raise/lower/def lines, is now in a data file I can easily update without code updates.
* The config file and the core engine now support:
    * Giving individual defs different priority, though right now they're still all priority 6.
    * Smarter handling of prerequisites (raise X before raising Y).
    * Smarter handling of mutually exclusive Performance skills.
    * Smarter handling of defs that require getting something out (e.g., wetfold) or outrifting (e.g., argleblaster).
    * Smarter handling of defs that can be done better ways depending on skillset (e.g., acquisitio).
    * Smarter handling of defs that require a location (currently just throne but now it could be others).
    * Smarter counting of wonderpie charges so as to skip a def if the charges are used up.
    * Smarter handling of pseudodefs about equipped gear (armor, mask, and circlet).
    * Support for defs that require a terrain (though none are yet loaded).
* GMCP is used for loading skillsets instead of screen-scraping AB.
* Now supports noting you have a yoyo, so acquisitio will use Rituals if you have it, then yoyo if you have that, and only then use an enchantment if you have neither. The `yoyo` def is gone, just use `acquisitio`.
* More configuration items, and a more extensible way for me to add more in future.
* Fixed a handful of bugs and did some performance improvements and code cleanups.
* Wonderpie defs are named `wonderpie<type>` to be consistent with other wonderitems (used to be `<type>pie`).
* Renamed the `robes` defense as `armor` since most of you folks aren't using robes.
* Code cleanup throughout. I used to be terrible at Javascript. Now I'm merely mediocre at it. Hopefully the code reflects that now.
* Found a handful of GMCP defense messages and built code to use GMCP for those, moving the old triggers into an Obsolete folder that will eventually get deleted. Need to find more of these -- I know most defenses don't have GMCP, but there must be more than the few I've stumbled upon.
* Added to the config data which defs are not lost on death, logoff, magic change, and in the def list, and revised the code accordingly, so that also can be maintained without code updates.
* Added more defenses.

## What next? ##

* I need information about other defenses, their triggers, config, and GMCP names if any, so I can add them. Hopefully others can start contributing that information. See the bottom of this documentation for a guide to how to help me get the info I need.

# Purpose #

Define profiles of defenses, then raise them -- without reraising anything already up -- and switch between them easily. Automatically reraise wanted defenses when they're lost. Handles classflex and skillflex intelligently.

Note: CrystalQueues and CrystalNotices are both required.

# Using Without Profiles #

Tell CrystalDefenses a list of defenses you want and then raise them:

* `DEFS WANT <def>`: add one defense to the current list.
* `DEFS UNWANT <def>`: remove one defense from the current list.
* `DEFS SET <def...>`: replace the current list all at once.
* `DEF`: every time you check your defenses, CrystalDefense jumps in and captures it, including a few other key commands.
* `DEFS`: see the defenses in your current list, in a single line.
* `DEFS WANTED`: see the defenses in your current list, in a color-coded grid.
* `DEFS RAISE`: raise any defenses in your current list you don't have up yet.

# Using Profiles #

Create as many profiles as you like for different situations. Note that some profiles, if present, will automatically be used by CrystalInfluence and CrystalDebate (see their documentation). You might also have profiles for hunting in different places, for combat, for standby (combat expected, but don't raise things that drain), and if you have other ideas, share them!

* `DEFS PROFILE MAKE <profile> <list>`: Create a profile with its complete list.
* `DEFS PROFILE SAVE <profile>`: As above but uses the list you already made.
* `DEFS PROFILE REMOVE <profile>`: Delete a profile previously created.
* `DEFS PROFILE ADD <defense> <profile>`: Rewrite a profile to add a single defense.
* `DEFS PROFILE QUICK <profile>`: Shows a quick view of a profile, ideally to copy and paste to make a new one or change the existing one.
* `DEFS PROFILE LIST`: List all the defense profiles you've made
* `DEFS PROFILE SHOW <profile>`: Show which defenses are in a profile
* `DEFS PROFILE <profile>`: Loads a profile into your current list without acting on it.
* `DEFS RAISE <profile>`: Loads a profile into your current list and then immediately raises anything you don't have up.

# Skillset Dependencies #

Whenever you are specifying a defense, you can make it dependent on a skillset by preceding it with the skillset name and a colon. For instance, you could list `acrobatics:elasticity` in the list. If you happen to be skillflexed to have Acrobatics, the list will now try to raise and maintain Elasticity, but if you are either skillflexed or classflexed to something without Acrobatics, it can stay in the list but will be ignored.

This lets you set up a single list that works in multiple skill configurations or even multiple classes. For instance, my `astral` profile includes some things I always want (e.g., `yoyo`) and some from various skillsets I might have (e.g., `acrobatics:balancing`, `harmony:lumphet`, and `illusions:illusoryself`). That way I can raise it and know I have what I need for hunting Astral regardless of my current skill configuration.

# Configuration #

A few configuration items control the functioning of CrystalDefenses (but there should be a lot more).

* `DEFS CONFIG`: See your configuration.
* `DEFS CONFIG <item> <value>`: Set a configuration item.

Some of the notable configuration entries:

* `Reraise` (on or off): Should lost defenses in the wanted list be reraised automatically? This accounts for many stripped defenses messages as well as death, logging off, and switching your magic type.
* `ThroneRoom` (a room's vnum or `here`): If present, and you try to raise `throne` it'll only happen in that room.
* `Container`: The name of the item (backpack, purse, belt, etc.) where you keep things like origami used in some defenses.
* `BeautyMaskID`, `BeautyMaskDesc`: The ID (e.g., `mask12345`) and short name of your Mask of Esteemed Beauty, if you have one. This is used to keep track of if you're wearing it so you can have defense profiles make sure it's on when you use those profiles. The same principle applies to `Circlet` for your Circlet of Lucidity, and `armor` for whatever kind of armor you wear (to avoid those embarrassing slips when you show up at a battle or a hunt wearing only a cocktail frock).
* `Yoyo` and `KissyFish`: If they have a value, acquisitio and waterbreathe will use the corresponding artifact (necessary since you can't just use the normal `rub` command like you can with most enchantment-replacing artifacts).

# Known Defenses #

These are the defenses it currently knows, and what they're called:

* Discernment: nightsight, thirdeye, lipread, aethersight, deathsight, powermask
* Combat: keeneye
* Lowmagic/Higmagic: yellow, grounded, blue, yesod, geburah, netzach, hod
    * [note: grounded will be red or malkuth depending on your current magic]
* Discipline: selfishness, insomnia, obliviousness, metawake, attune, truehearing, sixthsense
* Influence/Dramatics: charismaticaura, performance, sycophant, vagabond, bully, gorgeous, bureaucrat, drunkard, sober, genderbend, wounded, feigndeath, diplomat
    * preclusive performances will automatically switch between one another
* Music: bardicpresence
* Acrobatics: adroitness, limber, balancing, falling, elasticity, hyperventilate
* Illusions: changeself, invisibility, reflection, blur, illusoryself, phantomarmour
    * changeself tries human, and if you happen to be human it then tries taurian.
* Kata: grip
    * But not the equivalents in warrior and other skills, yet!
* Athletics: vitality
* Artifacts/Gear: throne, mask, circlet, armor, cloaking
    * throne only tries to raise if you're in the configured room
    * mask and circlet refer to the Esteemed Beauty and Lucidity items, so you can make your profiles put these on and take them off automatically when doing the appropriate activity
    * armor is to help make sure you don't accidentally go into hunts or fights without your armor
* Wonderitems: wondercorn, wondercorn<power>, hardsmoke, cleats, wondermask<power>, wonderpie<type>, wonderpipe<power>
    * wondercorn is the power that activates all of it at once
    * others with a `<power>` use the listed name, e.g., `wondermaskregal`
    * wonderpie automatically skips the power if it's out of charges
* Potions: fire, frost, galvanism, quicksilver, dragonsblood, musk, jasmine, vanilla, sandalwood, argleblaster, kiwipunch, goldentonic
* Herbs: rebounding, kafe
* Bookbinding: wetfold, kirigami, protection
    * The origami are expected to be in the Container configured item
* Enchantments: tempo, mercy, waterbreathe, waterwalk, levitate, beauty, cosmicnimbus, kingdom, acquisitio, perfection
    * If you have the Cosmic or Ritual skill it will use the spell instead of the enchantment for waterwalk, acquisitio, or cosmicnimbus
    * No attempt is made to recharge -- buy regulators!
* Rituals: draconis, fortuna, populus, rubeus, soulguard, cloak, timeslip, quickening, enthrall, goodluck, gnosis, chaotesign
* Transmology: fleshwork, fleshlegslengthen, fleshnoselengthen, fleshtorsowings, fleshaurastable, fleshskinmarble, flesheyesdarken, fleshearsscales, fleshbellyharden, fleshhandsgiant, fleshmouthscales, fleshnoseharden
* Elementalism: stoneskin, levitate, waterbreathe, elementshield, fireproof

# Defenses On Login #

Put code like this into your `onLoad` to automatically queue up loading a particular profile (in this case one name `basics`) when you log in:

```javascript
setTimeout(function(){ 
  set_variable("pCDDontRaiseDefenses","0");
  send_command("defs raise basics", 0);
}, 5000);
```

# Contributing #

If you would like more defenses to be added, the best way to do that is to tell me about them. Here's the full list of things I need to know to add a defense.

* **name**: Be sure the name you pick is obvious and unique and, if possible, it should be the same one Lusternia uses, or might someday use, in GMCP. Names should be short and made of only lowercase letters.
* **require**: What balances or other requirements as per CrystalQueues does this require? Your options in brief (full details in the CQueuesInstructions.md) include `bal`, `eq`, `larm` `rarm`, `lleg`, `rleg`, `head`, `psiid`, `psisuper`, `psisub`, `beast`, `slush`, `ice`, `steam`, `dust`, `heal` (all elixirs), `module`, and conditional requirements for health, mana, ego, and power like `p>=4` or `m>=max`. String multiples together separated by spaces.
* **consume**: Same as require, only what it consumes (this doesn't support the health, mana, ego, and power ones).
* **command**: The basic command to be executed to raise the defense. Try to be sure the way you write it will work for everyone. If it should use a config entry, use that with a dollar sign; e.g., `wear $BeautyMaskID`. (You might need to propose a new config entry too.)
* **altcommands**: One or more alternative commands that should be used only if you have a particular skillset. For example, if you happen to be an Elementalist, why try to rub a waterbreathe enchantment when you can just cast the spell? So waterbreathe has a `rub waterbreathe` command, but an altcommand of `elementalism: cast waterbreathe`. You can even multiple altcommands; since Lowmagic Red and Highmagic Malkuth give the exact same defense, they have a single entry with no command but two altcommands.
* **priority**: By default everything uses priority 6. If you feel a particular defense should be higher or lower, say so.
* **prereq**: This defense must be raised first. Example: `performance` is a prereq for things like `sycophant`.
* **performance**: Will be "true" only for skills that are part of Dramatics and performances, so that CrystalDefenses will know that if any other performance is already up it has to `perform end` first before performing this one.
* **getitem**: If raising this defense requires getting something out of a container (e.g., wetfold), specify the noun that should be gotten out (as with command you can use a config entry with a dollar sign).
* **outritem**: As getitem, but for items that have to be outrifted (e.g., argleblaster).
* **location**: If this defense can only be raised in a specific room (e.g., throne), specify the config name for the room (typically the same as the defense name).
* **terrain**: If this defense can only be raised in a specific terrain, specify the terrain name.
* **preserved**: Most defenses are lost on death, on logoff, and whenever you forget Lowmagic or Highmagic. If this one isn't, specify which of those three things it isn't lost on. In addition, a very few do not show on `def` or any of the other things that CrystalDefenses do after doing `def` (e.g., the cloaking of a Gem of Cloaking); if this is one of those, add `list` here, and CrystalDefenses will not clear it before rebuilding the defenses list.
* **gmcpname**: If Lusternia sends notifications about this defense being raised and lowered, this is the name it uses.
* **raise, lower, and def triggers**: The lines that tells CrystalDefenses when the defense is gained and lost, and what it looks like on the `DEF` list (or some other list, see below).

You can see examples aplenty here, though BitBucket combines all the lines instead of showing them neatly indented:
https://bitbucket.org/HawthornThistleberry/crystalship/wiki/Defenses

We might well need more than this. Some things you might need to tell me about:

* This doesn't show up on `def` but does on some other command. For example, Harmony mantras appear on `mantra status`. So I need to make CrystalDefense know that when you have typed `def` and it is rebuilding the defenses list, it needs to check there, too.
* Some defenses act like a package of a bunch of others. For example, `wondercorn activate all` shows up as six other wondercorn defenses, so there is special code to have a `wondercorn` defense that does the activate all option, but which gets listed as raised if all six of the other defenses are simultaneously present.
* Some defenses can only be raised in groups. I know Harmony mantras are like that. I will need to know how they work so I can figure out how to smartly code CrystalDefenses to handle them.
* You might need a new config item to specify something that will be used in the command, location, or getitem fields. I need a name and brief description like those in `DEFS CONFIG`.
* And then there's what I don't know that I don't know. What is unique about the way your defenses work that CrystalDefenses needs smarts to handle? I need you to teach me.

CrystalDefenses takes a slightly broad interpretation of what is a defense (as you can see from ones like `circlet`) -- so basically, if you can make a case that a particular thing:

* a) might make sense to have part of a profile
* b) can be captured in both raise/lower triggers and either `DEF` or some other command that running `DEF` can trigger
* c) would be useful for more than just you but other CrystalDefenses users

then I might well consider including it. I just need you to give me the information above and I'm happy to add it.

Working collectively we can make this a really thorough package. But I can't get the information without hearing it from you. Find Lendren on the Lusternia Discord in the #nexus-support channel and we'll arrange a way to get me all the data I need to add your defenses. Same thing if you find that some I already have in there are outdated -- it's been years since I played anyone who could `fleshcall`, for instance, so I wouldn't be surprised to find my list is out of date.

I'd also love to hear about any GMCP names for defenses I don't already know the GMCP names for. Come on, you must be seeing some things showing up in your status pane that I never see!
