# Setting Up For Debate #

Before your debate, CrystalDebate needs you to do a few things.

* You need CrystalQueues and CrystalNotices installed already.
* If you're using CrystalDefenses, create a profile named `debate` and load it with whatever you want to use in debates. If it exists, CrystalDebate will activate it at the start of debates. You can also create multiple profiles and use `DEB SET PROFILE <name>` to switch to using that profile (useful for Justice!)
* `DEB HELP` will show you a few options to set. Turn `ROULADE` on if you are a bard and you want to use Roulade to supplement your ego healing (it'll happen when below 30%); turn off if you're not a bard, or you want to avoid using it because it's not a fair fight. Turn `RUN` on if you want to automatically try to flee one room in a random direction if your ego drops below 20%. Turn `CHASE` on if you have artifact ears and you want to try to follow your target if they run away (only works with ears at this time, there are just too many exit messages and too many ways to conceal exits otherwise).
* Have a focus (powerful mind) enchantment. CrystalDebates assumes you have one.
* Turn on autocuring or have something else that'll do ego potions, scrolls, etc.
* Ideally, have the relevant Dramatics skills (if not, don't use the keybinds).
* Have your ego, ego regeneration, and ego curing as high as possible. That and luck are most of what wins debates.

# Debating #

To start debating someone, simply type `DEB <target>`. You can stop a debate with `DEB NONE` or `DEB END`, though it usually should do that automatically.

CrystalDebates will automatically do everything until the debate ends. Well, almost everything, see Keybinds below. Things it will do:

* Semi-randomly change mindsets every balance. It will keep cycling mindsets with a random factor so opponents can't pick up a pattern, but it also does so in a way that favors mindsets that benefit you based on your current attitude and afflictions, as well as any predictions of the opponent attacks, if any clear pattern has been shown.
* If you have `DEB SET ROULADE ON` and you drop below 30%, roulade yourself.
* If you have `DEB SET RUN ON` and you drop below 20%, run one room chosen randomly from the exits available.
* If your opponent just fled, try to chase them. This is very limited: if they move more than one room before you get balance back, for instance, it's going to be on you to use your own tricks to find them and start debate up again.
* Otherwise, semi-randomly do a debate attack. It cycles attacks with a random factor so your opponent can't pick up a pattern, but it also favors attacks that your current attitude benefits, as well as any predictions of the opponent's mindset, if any clear pattern has been shown.

## Predictions ##

A few notes on the predictions used. CrystalDebates keeps track of what attacks and mindsets the opponent has used, and in what order, and does a few simple, quick analyses on them, once it has enough of them. These are the patterns it looks for:

* **Final Repeat**: The last three or more were identical. This one is good for people who aren't cycling at all.
* **Heavy Use**: If one option is used more than half the time, predict that one, weighted by how much.
* **Repeating Triplet**: If someone cycles through all three in the same order, predict based on that, depending on how many repeats.

If a pattern is detected, CrystalDebate will weight the random choice according to how certain it seems to be, but still has a random choice, and all outcomes are always possible. This should make it much harder for you opponent to predict your moves -- even if they're using CrystalDebate. I may improve this prediction algorithm over time as I come up with other patterns to look for. But I intend always to have it just be a factor mixed with randomness, to minimize how well other people can deliberately exploit it.

## Keybinds ##

There are two things it won't do automatically that are your job, but it tries to help you do them, by providing keybinds.

### Fasttalks ###

Try to stick your opponent with debate afflictions. Note that these will typically be cured pretty quickly, between attitudes and focus pendants, but tossing one in every so often isn't a bad idea.

* **Alt-1**: Hurry (weakens the cautious mindset)
* **Alt-2**: Circuitous (weakens the analytical mindset)
* **Alt-3**: Loophole (weakens the pedantic mindset)

### Attitudes ###

Press one of these keys to set an attitude. You might want to do this before the debate begins, since they take a balance. They also cure afflictions; in fact, CrystalDebate will automatically notify you if you are afflicted while off focus balance and suggest a keystroke that will get that cure in, but it doesn't do it automatically because you might just want to ride it out until your focus balance comes back.

* **Alt-4**: Lawyerly (strengthens pettifoggery; cures loophole)
* **Alt-5**: Saintly (strengthens pontification; cures hurry)
* **Alt-6**: Zealotry (strengthens passion; cures circuitous)

### Justice Seal ###

If competing for the Justice seal, be sure to:

* Copy your DEBATE profile to a new one named JUSTICE and remove from it anything that is not allowed that year.
* `DEB SET PROFILE JUSTICE`
* `DEB SET ROULADE OFF`
* `DEB SET CHASE OFF`
* `DEB SET RUN OFF`

CrystalDebates won the Seal of Justice in 2024 with no practice, no preparation beyond the above (and not even the profile thing -- I added that in afterwards after I messed up my first try because the old version reactivated defenses I had carefully turned off), and almost no intervention by me (just a few hits on Alt-num keys now and then, probaby unnecessarily). But you'll do better if you've debated people before, so you already have some data about their patterns. If they have patterns.
