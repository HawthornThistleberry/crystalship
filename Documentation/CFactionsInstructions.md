# Factions List #

CrystalFactions maintains a table of names and what faction those people belong to. This information is fetched from the Lusternia API, but this is slow, so the information is cached in a table. That means if people change allegiance, the cached information can be out of date; for this reason, CrystalFactions also provides a means to quietly update the list in the background.

Note that CrystalFactions requires both **CrystalNotices** and **CrystalQueues**.

## Looking Up Faction Membership ##

If you want to look up someone's faction, just type `FACTION <person>`. If it's already in the database, you'll get an immediate response: `Lendren is of Serenwilde`, color-coded. If it's not already in the database, it'll send an API call. When this doesn't work, you can `FACTION SET <name> <faction>` will manually update your faction database. Once it's been set, future checks won't need to do any of this.

If you suspect they've changed allegiance, try `FACTION <name> FORCE`. This will force it to do the API lookup either way, and if it succeeds, update the table.

If the player is gone from the game or has renamed, use `FACTION <name> REMOVE` to remove them from the database entirely.

When you first install CrystalFactions, it will load a table of faction information I've prepared, currently at over 500 members as of this writing. You can force it to reload from this table with `FACTION LOAD` any time, in case I've added something. Doing so will not replace any faction membership you already have recorded, though, only add to it.

## Updating Faction Membership ##

CrystalFactions contains a command to help you update faction membership when it changes, semi-automatically. By running the command `FACTIONUPDATE`, you ask CrystalFactions to take one name off its list and re-check it. Just one. It does its work silently, and will even do a `REMOVE` if the API reports the character does not exist.

So how do you use this? You do not want to run this command a thousand times in a row. Not only would the multiple faction calls to the API collide with one another, that's very rude to the Lusternia server, and if people start doing things like that, they'll take the API away from all of us. Instead, you should run it regularly in the background, not more than once per ten minutes.

For instance, if you have some kind of loop that does something every ten minutes, add `send_command('factionupdate',0);` to it. Or you could set up a trigger on midnight happening (`It is now the`...) to do the same thing. For advanced users, consider making a loop like this in your **onLoad**:

```javascript
if (client.tenminuteloop == undefined || client.tenminuteloop == null) {
  client.tenminuteloop = setInterval(function() {
      send_command('factionupdate',0);
  }, 180000);
}
```

## Automatic Faction Lookup ##

By setting `FACTION AUTO <minutes>` (10 is a good option), CrystalFactions will automatically look up faction membership for anyone it senses but does not know already, but not more often than once per that many minutes (to avoid flooding the API server). Note, though, that it is fairly conservative about doing this. For instance, names at the start of lines are colored, but other words can be at the starts of lines, so it won't try to look up names at the starts of lines. It'll only do an automatic lookup if it's pretty sure what it's seeing is a name (e.g., if it sees a name on a `SCENT` output), plus the output of `QW` (and if you run `QW` every so often, that'll also help it always have names colored). Set the value to 0 to disable automatic lookups entirely. The default is 10.

## Color Coding ##

### Where It Happens ###

Some faction coloring approaches try to color-code everywhere a name appears. If combat is your primary focus, this may be important to you. My preference is not to go that way. Checking every single word that comes across your screen to see if it's a name is full of inefficiences and problems. You can get 95% of the benefit for 5% of the performance hit by looking for names in specific places where you can count on a particular word being a name in a consistent trigger, and where you really want to know about the person's allegiance. For this reason, name highlighting is limited to the following:

* `QW`, `WHO`, and `WHO HERE`
* When they speak on channels (clans, market, newbie, etc.)
* Aethersight when being scried
* Sensing people moving in adjacent rooms with artifact ears
* Scenting people
* People entering or leaving the area (e.g., spectacles)
* Tradewho lists
* Domoth challenges
* `LISTOFFERINGS SUMMARY` and `ORDER` (thanks to Auraelia for making me add this...long story!)

More locations may be added; if you have specific trigger lines that would be good ones to pick a name out of, send them to Lendren to get added. Note, though, that anything that's player-customizable, like the ten thousand "enter the room" messages, or even way too many 'sends you a tell' messages, are probably not worth trying to capture.

### What Colors Are Used ###

When identified, names are colored as follows, by default:

* Serenwilde: light green
* New Celest: cyan
* Glomdoring: dark orchid (a darkish purple)
* Magnagora: crimson
* Gaudiguch: orange
* Hallifax: white
* No faction: olive drab
* Not in faction list: no color change

I deliberately don't use red for either Magnagora or Gaudiguch because red is used for enemies and target, and because it's hard to easily distinguish too many shades of red. And anyway, flames are a lot more orange than they are red.

If you want to change these colors, you can. Note, though, that we're not talking MUD color codes, we're talking HTML color codes. The easiest way is to use one of these color names: https://www.w3schools.com/colors/colors_names.asp but you can also use HTML-style RGB codes like #8B008B from any color picker. Yes, you have more colors available than ANSI256, use this power wisely.

* `FACTION COLORS`: show the colors you're currently using
* `FACTION COLOR <org> <color>`: set a color

For instance, `FACTION COLOR Celest DodgerBlue` if you want a slightly darker blue. Or heck, go wild. `FACTION COLOR CELEST #FF1493` if you want to go the opposite direction. Have fun.

### Background Colors ###

If you use `CONFIG COLOR` to set things to have background colors that CrystalFactions may colorize (e.g., channels), due to a bug in the Nexus function for colorizing lines, this can cause messed-up colors in the rest of the line. The only way I know of to avoid this right now is to not use background colors in your channels. I have reported this to Nexus Feedback but I don't expect much to come of it.

### Enemy Coloring ###

In some cases, an additional check is made against your enemies list, or against the variable sCombatTarget (which is what I use for targeting in combat -- and if you use it too, this'll also work for you!), for additional highlighting:

* Aethersight: color the whole line red if it's an enemy
* Ears sensing people in adjoining locations: red if enemy or target
* Spectacles sensing entering or leaving the area: red if enemy

# Ally and Enemy Lists #

CrystalFactions also stores your ally and enemy lists. One reason is so it can do the above. These are also useful things to have available in scripts generally. I have many things that automatically do or don't do things based on whether someone is an ally or an enemy.

## Loading The Lists ##

When you first load CrystalFactions, it will automatically do `ALLIES` and `ENEMIES` once to load the relevant tables. From then on, it should automatically update as you ally and unenemy people, though it might not catch sneaky allying and enemying combat tricks (e.g., powers that sneak onto and off these lists, lust, etc.) so doing those commands again once in a while isn't a bad idea.

## Reloading The Lists ##

Sometimes you might get your enemy list or even your ally list wiped by some trickery like a mind-control `UNENEMY ALL` or `UNALLY ALL`. In this case, you can restore the list to whatever CrystalFactions last saw, using the command `REENEMY` or `REALLY`. (Yes, that's really the `REALLY` command. It's not really 'really', it's really 're-ally'. No, really.)

Note that this is not combatant-grade, just a convenience. For instance, if you should happen to have looked at your `ENEMIES` and saw it was blank, it's too late to `REENEMY` because CrystalFactions already cleared the list automatically. For that, you need to save them in name lists which can be like profiles; see below.

## Checking The Lists ##

To check these lists in your own scripts, use code like this:

```
if (get_variable('lAllies').split('|').includes(name)) send_command('yay ' + name);
if (get_variable('lEnemies').split('|').includes(name)) send_command('boo ' + name);
```

# Name Lists #

Name lists are just that -- a list of names you can save for later use. You can 'take a picture' of your current ally or enemy list, or specify a list of people manually, to save. Later, you can use these to load into your ally or enemy lists. Unlike the `REALLY` and `REENEMY` list, these won't ever change until you change them. Use this to save your enemy list, clear it for a wargame, then restore it. Or to swap around different ally lists for different events. Go nuts with them.

* `NAMELIST <list> ALLIES|ENEMIES` saves your current allies or enemies list in a list you give some name, e.g., "MyEnemies" or "Wargame" or whatever.
* `NAMELIST <list> {<name>}` lets you build a list manually. You just got assigned to fight alongside Ace, Flash, and Speedy in the wargames? No problem. `NAMELIST Wargames Ace Flash Speedy` will set you up a list for that.
* `NAMELIST LIST` and `NAMELIST SHOW <list>` to peek at the lists you have.
* `NAMELIST ALLY|ENEMY <list>` will wipe your current list, then load the specified list.

It's not a bad idea to take a snapshot of your ally and enemy lists once in a while into something like "Myallies" and "Myenemies", e.g., before you go into a fight, in case people try to dominate you into changing them.

