# What CrystalShop Does #

CrystalShop helps you run multiple shops. For each shop, you can specify a list of items and the desired stock quantity, as well as pricing information. With one command done in the stockroom you can take a full inventory, including both normal items (like jewelry or furniture) and special items (like herbs, keg refills, cube charges, and esteem). Another command shows you a list of what needs to be stocked to bring your shop back up to your desired quantities. There's also a command to help you stock regular items which will find them in your inventory, drop them, price them, and update your inventory automatically.

CrystalShop never looks at your shop log. It doesn't help you craft things or calculate prices. It has no idea how much profit you've made. It doesn't help you to avoid theft. It has no idea where you can find a cosmic enchanter, or what the going price for infused platters is. It's entirely incapable of helping you kill your enemies. All it can do is help you know how much you have in your shop and what you need to stock, and then help you price them. Ask your doctor or pharmacist if CrystalShop is right for you.

# Setup #

## Setting Up Shops ##

CrystalShop supports running multiple shops, each with its own separate stock levels, pricing, and inventory. Shops are kept totally separate from one another. At any given moment, only one shop is selected, and all CrystalShop operations apply only to that shop.

The first step is always to create a shop. All you have to do is give it a single word name to create a shop:

`SHOP CREATE starhaven`

Don't use any weird characters in the shop name, stick to letters and numbers.

This creates the shop and also selects it. At a later date, you can also select it to switch between shops:

`SHOP SELECT starhaven`

If you only have one shop, you probably won't need to do anything more than create the shop and then ignore this function.
If you run multiple shops, you may wish to make a trigger that automatically selects a shop once you enter its stockroom. That way, when you enter the stockroom, you never forget to select the right shop. (It would be a big mess to inventory one shop while another shop is selected.) Don't create this trigger inside the CrystalShop package, put it in your own triggers. Use the room name for the stockroom as the trigger text, and for the action, set it to Execute script and make the script:

```
send_command('shop select starhaven',0);
```
 
## Set Up Your Items ##

The setup for a shop consists of a list of items that you regularly keep stocked. For each item, you specify a 'need' (how many you want to have minimally stocked) and a 'pricing' (what price command to use). Some items are handled a bit specially due to how Lusternia supports them, but you define them all in CrystalShop the same way.

First, you create the item in the list. This same command will select it for further editing if it already exists in your list.

`SHOP ITEM a bunch of yellow freesias`

The name you provide, for most items, should be precisely what you see in `IH` or `II`. That may mean different things for items that have infusions or enchantments. Here are some examples of items you should be careful to list this way:

```
an origami package in the shape of a rainbow (action origami)
a smooth pearl ring (enchanted with perfection)
```

Note how this will be different from how it shows in `WARES`:

```
perfection (a smooth pearl ring)
actionorigami (an origami package in th
```

You always want to use the former, not the latter, version, or CrystalShop won't know how to stock or inventory the item. More about this for special item types later in this document.

When you do this command the first time, it will add the item to the list of items that this shop stocks, with a need of 0 and a price of 100, and no note. You can change this by following with these commands:

* `SHOP NEED 2`
* `SHOP PRICE 150 bin 7`
* `SHOP NOTE design 12345`

**Need** means the quantity that, if it's lower than that, you want to be informed to restock. It's not the quantity you want to stock up to. For example, maybe you stock ten emerald rings, but you don't want to be bugged to restock them once a single one sells. So you set the need to 2, so once you get down to just 1, you'll be informed of how many to restock. At that point, you wouldn't just make one, you'd probably make nine more to get back up to 10 so you won't be bugged to restock for a while. CrystalShop does not keep track of that part, only the level at which you want to be bugged. (In some circles these are called the "low water" mark, which corresponds to CrystalShop's need value, and the "high water" mark, which CrystalShop doesn't take into account.)

**Price** means whatever should be in the pricing command when the item is stocked. You don't even need to set this for special items (see below). It will be used when you use the `SHOP STOCK` command. As you can see it can include bins or anything else that would normally go on the `PRICE` command.

**Note** is simply a note to yourself. It might help you remember how to make a new one, or who you get them from. CrystalShop ignores it, but displays it on item lists. Setting the note to none or remove erases it. Only 12 characters fit on the tables (but see below about how to see the full note). You can use `SHOP NOTE REMOVE` to remove a note.

If you wish to change the need or price for an item you previously set up, just use the `SHOP ITEM` command as before. If the item you name is already in the shop's list, that will simply select and display it (including the full note, if any). Of course you have to get the name precisely as it was before, or it will just create a new item. You can use `SHOP ITEM` without any item name to see what item is currently selected. Then do the `SHOP NEED`, `SHOP PRICE`, or `SHOP NOTE` commands again with the revised values.

You can also remove an item from the shop's listing with `SHOP REMOVE`. This is a permanent action (but you can always create it again).

## Special Stock Items ##

Because of the way Lusternia handles some items, they need to be handled slightly differently. Each of the following items has to be named in a specific way for CrystalShop to handle them, and in each case, the pricing is never used (as you'll see in the section about Stocking Items below), though you can still set it for your own reference if desired. The special items are:

* Anything stored in the shop rift: the name should be the same as what appears on `WARES`, e.g., `onyx`, `marjoram`, `auronidion`, `platinum`, `suporu`, `sulfur`, or `rawtea`.
* Esteem: should simply be named `esteem`.
* Enchantment Recharges (cubes): should simply be named `recharges`.
* Enchantments: as they appear in `IH` (e.g., `perfection (a smooth pearl ring)`)
* Origami: as they appear in `IH` (e.g., `an origami package in the shape of a rainbow (action origami)`)
* Fluids in kegs: should be named whatever shows in `WARES` before the `(keg refills)` part, e.g., `the poison crotamine`, `amber beer`, `kawhe`, or `a potion of galvanism`. Note that if the name is long enough that `(keg refills)` is cut off, this is still true. Also, if it doesn't appear at all and the name is cut off, just include what shows in `WARES`, e.g., `a wiccan green tea of sage and blackber`.
* Sorcelglass: the name of the resulting item, e.g., `an apple blossom vial`, but unusually, if it is cut off in the `WARES` list, you should cut it off similarly, e.g., `a tarnished silver vial et`.

For these items, the need value functions just like for regular items. The units are whatever shows in `WARES`: units of esteem, individual riftables like herbs, refills from kegs, and cube charges. In the case of cubes and kegs, multiples of the same item will be summed up, so if you have three cubes with 1000 each that'll be counted as 3000 in inventory. If you set your need to 200, you'd be prompted to restock (that is, recharge those cubes) when the total charges available in your shop drops below 200, whether that's all on one cube or spread over all three.

## Reviewing Your Shop Items ##

You can review what items you've added to your shop with the `SHOP ITEMS` command. On its own it will list the entire shop. You can also add text after the command to filter the list to things that include that text; e.g., `SHOP ITEMS ring` will show anything with 'ring' in the name; that includes things that are 'glittering', as you'll see in this example:

```
ID      Qty Need Description                                Pricing      Note
------ ---- ---- ------------------------------------------ ------------ ------------
88        4    2 a smooth pearl ring (enchanted with perfec 500 bin 3    
89        6    2 a bloodstone ring etched with salamanders  500 bin 3    
90        7    2 an etched garnet ring (enchanted with merc 500 bin 3    
91        6    2 a beryl sun ring (enchanted with beauty)   500 bin 3    
95        1    1 a glittering diamond watch (enchanted with 9500 bin 3   
158      10    1 a delicate strawberry meringue cookie      25 bin 9     
200       1    1 a package containing a covered fighting ri 5500 bin 5   7299
201       1    1 a package containing a dizzying rainbow sp 5500 bin 5   6345
279       2    2 a dancing firefly squid earring            250 bin 3    
------ ---- ---- ------------------------------------------ ------------ ------------
```

Here you can see that each item has its own internal ID (only used by CrystalShop; this doesn't correspond in any way to anything Lusternia uses). Generally, you can ignore this. However, if desired, you can select an item without typing its full name by using the ID. For instance, typing `SHOP ITEM 89` selects the bloodstone ring. (That's a lot quicker and more reliable than typing `SHOP ITEM a bloodstone ring etched with salamanders (enchanted with kingdom)` if you happen to know the ID.)

When you first start setting up a shop, the Qty column will be showing all zeroes, but don't worry, it's almost time to take inventory. First, you need to stock and price your items. You don't need to restock things already present in your shop when you first set this up, though, you'll find those in your inventory (in the next sections).

# Running Your Shop #

## Stocking Items ##

For special items as noted above (kegs, cubes, riftables, and esteem) it's up to you to stock these and set their prices. CrystalShop won't help here. That's because generally you do this only once. The keg, cube, and pedestal will keep their prices even when drained and restocked, and the rift remembers its prices as well.

For everything else, each individual object has its own price. Pricing them all is a pain, but CrystalShop has you covered. Simply have the items in your hand you want to stock, and be sure you don't have other items of the same type in your hand that you don't want to stock. You need to find a word that'll appear in all the items you want stocked; e.g., to stock all your flowers, if `IH FLOWER` shows you the right items, you're ready to go:

`SHOP STOCK flower`

CrystalShop will look at your inventory using `IH`, then any item that shows up in that list, and which has an entry in your shop's item list, will be dropped and automatically priced. Any item that isn't in your shop's item list will simply stay in your hands. CrystalShop also updates its internal inventory count while it's doing this, so you may not have to take your inventory again (see below). For example, it might look like this:

```
You are wielding:
   mandolin36163 : a scuffed mandolin bearing the stories of the road in your left hand.
   paintbrush116323 : a golden paintbrush in your right hand.
You are holding:
"rose247768"                            a brilliant white rose
"freesias245566"                        a bunch of yellow freesias
"rose152666"                            an elegant pink rose
"sunflower259189"                       a vibrant sunflower
You are wearing:
"ring35290"                             a ruby flower ring (enchanted with ignite)
"rose109317"                            an entrancing red rose
Number of matching objects: 6 (out of 81 total)

You drop a bunch of yellow freesias.

You set a bunch of yellow freesias to sell for 50 gold sovereigns each.  Bin 1. Total in stock: 14.

You drop an elegant pink rose.

You set an elegant pink rose to sell for 50 gold sovereigns each.  Bin 1. Total in stock: 5.

You drop a vibrant sunflower.

You set a vibrant sunflower to sell for 50 gold sovereigns each.  Bin 1. Total in stock: 10.
```

The same will work when stocking a bunch of rings or bracelets, a pile of origami, all your powerstones, etc.
When this happens CrystalShop is simply increasing the inventory by the amount it dropped. It doesn't know yet if you picked anything up, if anyone bought anything, etc. It also won't notice changes in rifts, kegs, cubes, esteem, etc. So this doesn't obviate the need to take inventory as described below. It's simply a shortcut to help in one common situation: where you're restocking and you're partway through and want to see what still needs stocking. You should always do a new inventory whenever you're not sure if anything else has affected the inventory, including yourself.

Note that when you first install CrystalShop, you don't need to stock the items already in stock. You'll be inventorying them soon. However, you certainly can. For instance, suppose you want to change the price on all of 50 rings. You've set up their new pricing in CrystalShop. Now just `GET` all of those rings, then `SHOP STOCK ring`, and it'll re-drop and then re-price (using the new price) those rings.

## Taking Inventory ##

Couldn't be easier. Just be sure you are in your stockroom before you do this command. If you do it somewhere else, the inventory readings you get will be utterly wrong and entirely baffling. (But don't worry. Just go to your stockroom and do it again. Each time you do this, it takes inventory completely from scratch.)

`SHOP INVENTORY`

This will scroll the hell out of your screen. Depending on how much stuff you have, and the speed of your computer, it may take a few seconds too. (In my shop with 724 items it takes about two seconds, but it might be longer for you.) You will see a number of lists scroll by very quickly.

What is it doing? It's counting every single item that's in stock. Well, not every item; it ignores any item that you never defined with `SHOP ITEM`. Also note that if it sees an item sitting on your shop floor (not counting the special items described above), it will assume it's stocked, even if you never priced it. Using `SHOP STOCK` avoids this situation, and it's also a good idea to periodically glance at `IH UNPRICED` to see if you have items you don't realize are unpriced.

Do this as often as you like. I generally do it first thing when I visit my shop. Paradoxically it's easier to use this than `SHOPLOG` to see what needs restocking, so don't bother to buy a shop log, or only buy one to see who's been visiting your shop.

At the end of the inventorying process, after a second, you will automatically see the results of `SHOP NEEDED` so you can get right to work on restocking (if necessary).

Note that once you've done this, `SHOP ITEMS` will show the inventoried quantities. And you can do that from anywhere in the world. If you're in your workshop and can't remember how many emerald rings you had last time you took inventory, you can type `SHOP ITEMS emerald` to see. It won't reflect any that got bought since you took inventory last, of course, but it's still useful.

## See What You Need To Stock ##

It's all been building up to this. Now you simply type `SHOP NEEDED` to see a list of precisely what needs restocking:

```
ID      Qty Need Stock Description                                           Note
------ ---- ---- ----- ----------------------------------------------------- ------------
12        1    2     1 a striped orchid                                      
59        0    1     1 a package containing a bed of thickly piled furs      beds 18873
100       1  100    99 esteem                                                
104       2   10     8 glowing ink                                           brewmeister
109       6   10     4 a potion of galvanism                                 lorecrafter
115       6   10     4 sandalwood oil                                        lorecrafter
126       6   10     4 the poison hadrudin                                   scorpions...
137       6   10     4 the poison dulak                                      gila liza...
------ ---- ---- ----- ----------------------------------------------------- ------------
8 items need to be stocked.
```

Now go ahead and make or buy those things, come back here, and `SHOP STOCK` them (or for special items, just stock them normally). For regular items, as you stock them, the inventory will update automatically, so you can just keep typing `SHOP NEEDED` to see how close you are to done. But as noted above, picking up items will get this out of synch, and it won't reflect special items. So periodically do a new `SHOP INVENTORY` and then `SHOP NEEDED` to keep track of where you really are.

Remember that the Stock column only reflects what you need to get to where CrystalShop will just barely not bother you about this item any more (i.e., the "low water" mark). If you're going to bother to get esteem, you probably don't want to stop at 99, or the minute someone visits the shop, you're suddenly back to needing to get more esteem. So stock up to whatever level you have room for (the "high water" mark).

What if you want to have the item get inventoried and be priced with `SHOP STOCK`, but not show up on `SHOP NEEDED`? (For instance, items you want to sell when you can get them, but which you can't reliably get, e.g., candy-box candy.) Simply set the need to `0` for those items. They'll still be listed in `SHOP ITEMS` so you can see your inventory, and you can still easily price them with `SHOP STOCK`, but they'll never get put onto your needed list.
