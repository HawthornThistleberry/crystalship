# Purpose #

This package is for Nexus (the Iron Realms HTML5 web client) to support aetherhunting in Lusternia. It provides, as of this writing, full support for manning collectors, turrets, and the empathic grid (command chair is built but not thoroughly tested yet) and works with the standard calls used by the widespread Mudlet packages (Dreadnought and Windmills) as well as some functionality of its own.

# Features #

* All the features of the other packages (Windmills and Dreadnoughts), plus:
* When using the grid, target by module name or number, or the person on the module.
* When doing a slivven check, identifies room names and numbers for known ships.
* Automatic aethertrading that gets the highest return possible at the trader.
* Turret includes a special mode for non-finks on ships with finks for higher efficiency.
* Tracks aetherwill and announces when it's running low.
* Slivven warnings.
* Can use scent to planarbond and covey all on the ship in a single command.
* Personality: customize the messages it shows, and that you say on ship channels.
* Easy power distribution by module name or number, or the person on the module.
* Gagging to keep the scroll manageable.
* Cycling siphon calls.
* Screen reader mode.
* Notifies you when a new version is available automatically.

# Installation #

**Important!!** While installing CrystalShip, you must disable the built-in `Numpad movement` reflex package. Just uncheck it on the Reflex Packages page. Why? When using CrystalShip in the command chair, in order for the numpad to be taken over for flying the ship, CrystalShip needs to be able to enable and disable the normal movement keybinds, but the default Numpad movement package Nexus comes with offers no way to do that. So CrystalShip includes its own copy of them, which it _can_ enable and disable. But that won't help if you have the originals there too.

__Advanced__: Suppose you want a customized numpad movement. No problem! Just make sure your own version of the package puts the keybinds in a group inside `Numpad movement`, and sets a variable called `sNumpadGroup` to the name of that group, and leave the package enabled after all. If CrystalShip sees a variable named sNumpadGroup, it will use that to disable that part of that package and then reenable when you unlock, rather than working with its own internal copy of the default package.

# Instructions #

This document explains some of the basics about how to use CrystalShip. Remember that any time `CS HELP` shows you the command that apply to you based on the module you're on. `CS HELP ALL` shows all commands regardless of module.

When you lock in, because CrystalShip needs information in the prompt, if it's turned off, CrystalShip will turn it on, then restore things to how it was when you unlock. If you normally run with prompts on, it won't do anything.

## General Commands

At any time you can look at `CS STATUS` (or just `CS STAT`) to see the status of CrystalShip and the ship itself. It shows your preferences, the hull health, and your aetherwill. Note that some of this depends on having the prompt on. CrystalShip does its best to identify what it can via GMCP and other prompts, though oddly, the ship maximum hull is never displayed anywhere(!).

Aetherwill drains as you do things (more so on collectors, less on grid, almost not at all on command chair) and when it gets below 50% things start to fail. Keep an eye on it. You can also announce over the ship aether what it is with `CS SAY WILL`; this will also happen automatically, but not more often than once every two minutes, when you start to get exhausted. Similarly, `CS REPLY WILL` does the same in a reply, in case someone asks in a tell.

There is a lot of spam in aetherhunting. `CS GAG ON` tries to make it more manageable by hiding things like the other turrets firing, and lots of miscellaneous events. But it still shows you things you want to know about, like what your crewmates are doing, and the arrival and departure of aethercreatures. If you want to see everything, `CS GAG OFF` but beware of the spam.

Aetherspace is especially challenging for people who use screen readers. CrystalShip tries to help as much as it can, though I can't think of any way it can help make up for the lack of that gigantic aetherspace map (hopefully you can fuse to a flashpoint, do the stuff, then fuse back to a berth, and avoid trying to navigate). If you `CS SCREENREADER ON` a set of triggers and functions will be enabled that play distinct sounds, and replace text with a very brief message, when these things happen:

* A vortex karibidean is spawned
* Any other creature is spawned
* A creature enters the same spot as your ship
* A creature explodes
* Shockwaves are about to run out

I'm eager to get input from screen-reader users about what else might be useful (particularly if those might differ on different chairs), and if the sounds are good or bad (they are chosen more to be distinct than appropriate, but it's hard for me to guess which are the best sounds for a screen-reader user to use). To turn this back off, `CS SCREENREADER OFF`.

When you're assigned to kill slivvens, it can be helpful to have a beep whenever one is announced. `CS SLIVVEN ON` will make an audible warning, and `CS SLIVVEN OFF` will turn it off. Of course you won't hear it if Nexus's volume slider is all the way down!

## Collector

### Being Assigned to Siphon Duty

When you're locked to an energy collector the important thing is that only one person be siphoning. If two people siphon at once, you draw too many creatures and can get the ship blown up. Because of this, for safety's sake, CrystalShip will always default to *not* siphoning `CS SIPHON OFF` whenever you lock in.

If the captain assigns you to siphon, you need to activate your siphoning manually with `CS SIPHON ON`.

There's a special mode for the rare case where you are both siphoning and slivvening (really only possible if you kill them very fast). `CS SIPHON AUTO` will work like `CS SIPHON ON` but with the possibly-dangerous difference that when you unlock and relock, it'll stay on `CS SIPHON AUTO` so you won't have to keep turning back on each time you go slivven and return. Use this with caution!

Some captains like to rotate through different siphoners, typically with a call like "Siphon 1". If you've been assigned the tag 1, type `CS SIPHON 1` and then you will respond only to the call "Siphon 1". Tags can be any kind of word or number; if the captain prefers to call you by name, use that. For instance, if you typed `CS SIPHON Roark` then you will siphon only when the captain calls "Siphon Roark".

### Drawing Power

The collector also is a source of power. Drawing power from the collector saves you drawing it from your nexus -- just make sure the ship has enough for its own operations. `CS DRAW <amount>` will attempt to draw that many power, and if it can't quite, it'll try one fewer, and so on, until it can. This is because reserves are double power, so you can't tell exactly how much you need to get to full. So if you're down 11% try `CS DRAW 23` because maybe you're short 23, or 22, or 21, and it'll try each one in turn.

## Turret

The default when you lock into a turret is to fire whenever the captain calls a target, that is, `CS FIRE ON`. You can turn it off with `CS FIRE OFF` if you're very sure you shouldn't be firing for some reason.

There's also a special mode called `CS FIRE NOTFINK` which is used if you're not a fink, but other members of the crew are finks with damage shift and cages. In this mode, you won't fire at the easier creatures like the scyllus; you'll only fire at dragons, tendrils, and karibideans. The point of this is to allow the finks to kill those to keep their critical hit damage shift going. I'm not sure how effective this is, or what effect it might have on your own experience gain, so use it with caution and report back what results you have. If you need to change the list of creatures that you target in notfink mode, use `CS FIRE NOTFINK <targetlist>`; e.g., `CS FIRE NOTFINK KARIBIDEAN TENDRIL DRAGON SCYLLUS` if you want to add Scyllus to the list.

## Empath Grid

### Repairs

While working the grid is much more complicated than the turrets, CrystalShip handles most of it for you automatically. Once you lock in, it will begin to monitor ship and module health, and do repairs automatically, according to a fairly thorough prioritization algorithm that should keep you alive in any aetherhunting situation that's not totally irredeemable. (It's not ready to handle ship-to-ship combat though.) This algorithm includes things like not healing unnecessary modules while there are aethercreatures around (so as not to waste a heal balance it might need more urgently a moment later), or to struggle to heal things fully if a slivven is still attacking them. It automatically does clarity on the command chair after every kill, and also when the pilot calls for clarity or emergency clarity. You can turn off the automatic after every kill clarity with `CS KILLCLARITY OFF`.

Usually you won't need to do anything about any of this. If you notice it isn't healing something it should, you can reset some internal variables and queues with `CS RESET` and it will generally resume where it left off, but this is very rarely needed, if ever. You can also force it to heal if it's not doing it. `CS HULL` will make it heal the hull, `CS SCAN` will make it scan modules and pick one to heal, `CS HEAL` will do the same thing but without the scan, and `CS HEAL <module>` lets you specify what to heal.

Note that when specifying the module, you can do it several ways. You can provide a full name (e.g., turret12345) or just a number (e.g., 12345) if you like. But you can also just type the name of the module (e.g., chair) and let it figure it out (note for turrets and collectors, it'll just do the first one). Best of all, you can provide the name of the person in the module and it'll figure it out on its own; for instance, if Tristanna asked you to heal her turret, `CS HEAL TRISTANNA` works without you looking up which one she's on.

Important note: if you lock in while the ship's hull is already damaged, because Lusternia won't show the ship's max hull anywhere, you might get a dangerously misleading idea of the ship's actual maximum hull. See the Known Issues section below. This will rarely be an issue.

### Slivvens

Empaths are also responsible for checking for slivven and reporting their locations to the crew. CrystalShip also does this automatically, whenever a slivven invades, or anyone asks for a check on the ship aether. You can also force it yourself with `CS CHECK`. CrystalShip knows the layouts of the aetherships Songbird, Tristree, and Dreadnought, and will report not just the modules but their room names and path numbers, helping slivveners find the specific module easily. On other ships, it'll only report the module.

By default, CrystalShip will use a concise report for slivven checks that fits the entire report into a single ship tell. You can set it to use a verbose report, with one line per slivven plus one for the count, with `CS CHECK VERBOSE`, or switch back with `CS CHECK CONCISE`.

On some especially large ships, some captains prefer to ask the empath to never heal any unoccupied but infected collectors, and the crew ignores them. Even in the default mode, CrystalShip only heals unoccupied collectors when there are no active targets, and then, at the lowest possible priority except for the orb. But if the heal balances you're spending in between fights healing unoccupied collectors that are being gnawed on by slivvens bothers your captain, `CS INFECTED OFF` will set CrystalShip to ignore any unoccupied and infected collector, and won't heal it even if it has literally nothing else to do. This defaults back to `ON` every time you lock in again.

### Communications

It's also the empath's job to set up communications. `CS LINK` will link everyone it can find; first, it'll link anyone it already knows is on a module (if people have locked in since the last time you did a `CS SCAN`, do another scan to update first), then it'll attempt to `SCENT` and link anyone it can find that wasn't already linked. If you don't have the Scent ability, you'll see a "That is not a valid command" error during this, and you might not link people who are not locked into a module, but at least you'll have linked everyone who was locked in as of your last scan!

`CS BOND` does the same thing, plus it also sets up planarbond and covey. Only use this if you have the FirstMate skill, and when the ship is launched.

### Power Distribution

The empath also distributes power from module to module. You can do this more easily with `CS DIST <amount> <from> <to>`. As with `CS HEAL <module>` you can specify a module any way that makes sense. For instance, `CS DIST 50 ORB TRISTANNA` saves you having to look up her module number, or the orb's for that matter. Or `CS DIST 25 LIEF TRISTANNA` to split the power between your finks.

### Trading

Empaths are also responsible for trading with gnome merchants. CrystalShip makes this easy. Once your captain has pulled alongside a merchant, simply type `CS SELL`. You'll take stock of your dust, check the wares, then make a series of buys and sells that gets the maximum possible rate of return for the dust you have, given the prices. (You could do better by buying from one merchant and selling to another, of course, but who has time for that?) If the trader moves in the middle of the process, just get alongside and type `CS SELL RESUME` to pick up where you left off. It's up to you to distribute the proceeds however the crew has agreed.

### The Priority Algorithm

You don't need to know this, but in case you're curious, here's the algorithm currently being used to prioritize the grid. It goes down this list and stops at the first one, every time it gets a balance. This algorithm is designed not only to heal the ship but also retain the ability to keep healing it, and to eventually heal everything, though some things are saved until outside combat, so you won't be off grid balance just when an urgent need comes up.

* if there is a manual override set to high priority, do that (emergency clarity)
* if the hull is at 60% or less, heal hull
* if the grid is heavily damaged or worse, heal the grid
* if the hull is at 80% or less, heal hull
* if the chair is heavily damaged or worse, heal the chair
* if the grid is light damaged, heal the grid
* if the chair is light damaged, heal the chair
* if there is a manual override set to medium priority, do that (clarity, grid join)
* if the hull is at 95% or less, heal hull
* if a turret is blown up, heal it
* if a turret is at critical damage, heal it
* if a turret is at moderate damage, heal it
* if there are currently targets being fired at, stop here; everything below this happens only outside combat, to save grid balances for more urgent things
* if a turret is at light damage, heal it
* if a collector is at light damage and uninfected, heal it
* if the hull is at 99% or less, heal hull
* if there is a manual override set to low priority, do that (planarbond, covey)
* if a collector is at light damage and infected/unoccupied, heal it (unless that is disabled)
* if the orb is damaged, heal that
* if there is any trading to be done, do that

Calls from the captain going into the manual override queues can thus interfere with CrystalShip's own smarts, because of course you have to obey the captain. In the past, this could mean a captain running a module that calls for clarity too much could keep the grid so busy with clarity that the ship could fall behind on healing. As of v1.a.5, CrystalShip has some smarts to selectively ignore a captain's excessive clarity calls to avoid this: it refuses to queue multiple sequential clarity calls (that is, if it's already going to do clarity when it can, it won't plan to do a second one), it clears all clarity calls whenever the ship tells it no clarity is needed, and you can set it (with `CS KILLCLARITY OFF`) to not queue clarity itself on kills, in case the captain is also doing that.

## Pilot

As always, `CS HELP` will show you the relevant commands while you're locked into a command chair. Note that most of the commands can be abbreviated, as indicated, to a single character, to make it possible to type them really fast. 

### Autopilot

CrystalShip has an autopilot mode where it will follow the common triangular path while calling for siphoning and targeting. As of this writing I'm not sure how thoroughly tested this is, but I would love to hear from people who've tried it. CrystalShip isn't (at least yet) meant to replace, but only to assist, an experienced captain. To use this, you'll need to set a path with `CS PATH`, then turn on automatic with `CS AUTO ON`.

By default CrystalShip will call for clarity during autopilot no matter what creatures you are fighting, but there is an option to change this: `CS SKIPCLARITY <list>` will set a list of creatures to skip the clarity call on. `CS SKIPCLARITY NONE` to return to calling for clarity no matter what creatures are attacking.

### Calls

Many of CrystalShip's pilot commands are really just shortcuts to make the calls to the other crew so you can do it with just a few characters. Notably, you can call for a shockwave, vortex, anchor, clarity, and emergency clarity, and manually call a target (though CrystalShip will generally handle calling targets for you under normal circumstances).

Whenever CrystalShip calls for a siphon, it can automatically cycle through numbered siphoners. CS SIPHONERS 0 will set it to use the classic call "Siphon". Otherwise, if you have five siphoners, type CS SIPHONERS 5 and CrystalShip will cycle between calling "Siphon 1" through "Siphon 5". This resets every time you lock into the chair or reset, so you need to set it again.

### Other Pilot Commands

A few things you can do yourself are scoop dust, turn on the forcefield, turn silent running on and off, transverse, and spiral. These are all just shortcuts to the commands you probably already know, for quicker typing.

### Flying With The Numpad

In addition, while you're on the command chair, your numpad is remapped to the common keys for movement and quick commands; see `CS HELP KEYS` to see a diagram of the numpad. Using this you can quickly steer the ship through most of the simpler actions.

## Known Issues

* Strangely, there is nowhere (not the grid commands, not `SHIP STATUS`, not even the prompts) that shows the ship's maximum hull. CrystalShip attempts to get around this through two tricks. First, when you initially lock in, it will capture the hull amount from `SHIP STATUS` and sets that to the maximum hull. This is almost always the right number because you're rarely locking in while the ship is damaged. Second, every time the prompt shows, if the hull is higher than what it thought the max was, it increases it accordingly. So if you do lock in while the ship is damaged, your prompt will fix it soon after. The only situation where this could be bad is if you lock into the grid in the middle of a fight and CrystalShip doesn't know to heal the hull because it doesn't know how damaged it is. In this case, type `CS HULL` once or twice. If you're not on grid, the maximum hull is only used to display ship status in `CS STAT`, so it doesn't really matter.

# Version History

v1.a: 2021-12-29
- Added siphoner cycling.
- Many fixes on autopilot, including different strategy for using movement, clarity, and shockwaves.
- A screen reader mode.

v0.9: 2020-11-08
- On load and CS HELP CrystalShip will notify you if there's a later version.
- Now uses color in most displays.
- Attempts to identify ship maximum hull when locking in.

v0.8: 2020-09-15
- Added CS SELL command that intelligently unloads dust for the best return.
- If the trader moves in the middle of the process, catch up and CS SELL RESUME.
- Added locations for Tristree to slivven check identification.

v0.7: 2016-11-19
- Added one missing Dreadnought location.
- Pilot module now supports autohunt, but not tested yet.
- Pilot numpad navigation, including move/glide toggle.
- Pilot aliases for many common functions.
- Warning if not all turrets are firing.
- Warning when shockwaves are due to expire.
- Personality system for setting custom messages. CS PERSONALITY to see and set.

v0.6: 2016-11-09
- Complete Dreadnought room names for slivven check, with path walk roomnums
- CS LINK now just does links; CS BOND does them and covey and planarbond
- Fixed a bug in the "Siphon off" call
- Warning if your collector is full when asked to siphon
- Directed to CS HELP when locked into a module
- Added link to hosting site and docs in CS HELP

v0.5: 2016-11-03
- Fixed bugs in grid handling in hunting due to real testing. Grid module is now ready to use!!!
- Added CS HULL to directly heal the hull, should never be needed though.
- CS CHECK CONCISE and CS CHECK VERBOSE to choose a format for slivven checks.

v0.4: 2016-10-28
- Respond to "Fire on all targets" by moving to CS FIRE ON even if on NOTFINK.
- Slivven warning bell: CS SLIVVEN ON/OFF
- CS STATUS shows 'status bars' for a quick glance of percentage values
- Grid healing implemented but not yet tested!
- CS LINK coveys, planarbonds, and links everyone in SCENT (required)
- CS DIST to distribute power between modules easily
- CS HELP only shows help relevant to where you're seated
- CS HELP ALL shows all help
- CS LINK to draw energy from collector (start with highest possible)
- Basic, temporary manual piloting keybinds to move, call targets, call siphon, and call clarity

v0.3: 2016-10-07
- Slivvens in rooms highlighted for easier location
- CS FIRE NOTFINK: A mode where you fire only at tough targets, leaving smaller ones for finks to build up streaks with
- Slivven check also reports room names when on Dreadnought
- Slivven check also shows room numbers for PATH WALK on Songbird

v0.2: 2016-10-05
- Uses GMCP to track aetherwill; CS SAY WILL to say in SHIPT, CS REPLY WILL to tell in reply
- Announce tiredness automatically, but not more than once per two minutes
- Siphon mode AUTO works like ON and stays on after lock/unlock, for when you're both siphoning and slivvening
- CS FIRE ON/OFF to man a turret but not fire (defaults back to ON when you lock in)
- Turret automatically chooses whether to target creature or ship based on name
- Beginnings of grid support: so far just a slivven checker, more to come!
- Improved efficiency and reliability

v0.1: 2016-09-30
- Support for manning a collector, with siphon on or off - CS SIPHON ON/OFF
- Siphoning defaults to off whenever you lock in, for safety
- Support for manning a turret, including vortex, bombard, and shockwave
- Automatically switch to targetting karibidean if spotted
- Gagging for aetherhunting spam, CS GAG ON/OFF
- A few basic slivven-hunting reflexes
- Works with prompt enabled or disabled
- CS HELP for help on commands
