# Purpose #

Automate the harvesting of herbs. And when I say automate, I do not mean you should be AFK while doing this. Don't blame me if you break the rules! This package is intendned to make it easy to harvest efficiently _without ever strip-harvesting_. It's not meant to help you get yourself shrubbed. If you do that, it's on your head.

# Installation #

Install CrystalNotices and CrystalQueues first (if you don't already have them), then this. As you install each thing, use the `CINSTALL` command, e.g., `CINSTALL HERBS`.

# The Basics #

## Harvesting In One Room ##

Go to the spot you want to harvest, then specify which herbs you want. For instance, `CHARVEST MARJORAM CHERVIL`.

CrystalHerbs will quietly keep checking the current level of the plants and then harvesting groups until the first herb drops below the target level, then move on to the next herb, and so on until it's done. What target level? See Target Levels below. When it's done, it'll pop up a notification and make a sound.

## Harvesting In Many Rooms ##

Go to the first spot you want to harvest, and start the harvesting as above. While it's happening, specify the path to subsequent rooms in which harvesting should happen. For instance, `CHARVEST PATH N NW W W NW S` to harvest here, then north of here, then northwest of there, and so on.

CrystalHerbs will harvest one room, then move to the next, and so on. It's counting on you to pick directions correctly. If you choose a path that doesn't work, it'll blindly try to move that way and harvest again anyway.

Each room on the path must be of the same kind of terrain or it'll stop, but it's okay to double back over a room, because when it gets back to a room it already harvested, it'll see the levels are too low and just keep going. So you could `CHARVEST PATH N S W E` and that'd work fine.

Note that in places where there are waves, earthquakes, winds, infernos, cows, or other things that disrupt your movement, CrystalHerbs does nothing to try to recover from this. If you don't have the appropriate artifact to not be so interfered with, your path might not work that well. Using a larger number of shorter paths can help.

You can also set a command to do when the harvesting is finished: `CHARVEST AFTER <cmd>`. This will be cleared each time it is executed.

## Aborting ##

CrystalHerbs will automatically abort if it finds a missing herb (see Replanting below). You can force it to stop with `CHARVEST STOP`.

Alternately, `CHARVEST PATH NONE` will let it finish the room it's in, but stop after that, by clearing the path.

# Target Levels #

CrystalHerbs is doing two things to try to be clever about how much to harvest:

1) By using the `GROUP` option, it'll harvest so much faster, but that means how many you pick on a single action is variable. Which means it's impossible to harvest to an exact number of herbs left behind. The only way to be sure you hit an exact number is to give up using the `GROUP` option at least partially, and that just slows things down too much, and for what? The important part isn't a nice round number, it's not strip-harvesting. So instead, CrystalHerbs sets a threshold and stops once you _cross_ it. And by using a threshold higher than the largest amount a group can get, it gains full benefit of group harvesting without ever strip-harvesting.

2) CrystalHerbs knows what plants are in season when, and will automatically adjust the target number up when a plant is farther from its peak, to leave more of a buffer through its slower season and its hibernation. By default the multiplier is 2. What that means is, if it happens to be peak month, it'll harvest until the remaining plants drop below 10. If it's one month off peak, the target level adjusts to 12. If it's two months, it's 14, and so on, to a maximum of 22 when it's six months off peak -- that is, hibernation month.

You can change these numbers easily. `CHARVEST TO 10 2` sets the default, but change as you see fit. You can make it more sensitive to the season with `CHARVEST TO 10 3`, or even more; `CHARVEST TO 10 10` would basically mean it won't even touch anything during hibernation month (but you won't get much the rest of the year either). You can leave more buffer with `CHARVEST TO 15 2`. You can disable the whole 'adjust by the season' thing by setting the multiplier to 0.

But if you change these values and this results in you strip-harvesting, don't complain to me! Replant, change the values back, and be more responsible next time.

## Hibernation ##

There is a persistent but unfounded rumor that any harvesting during hibernation month can make a plant go extinct in a location. In fact, there's a chance of a plant going extinct in a room in hibernation whether anyone touches it that month or not. During hibernation month, there is a chance each day of a certain number of the plant dying off. If the number of plants in that room is low, that can mean it goes extinct in that room. Whether it got brought to that low level on hibernation month or the month before or three months before doesn't change that, except for the probabilities that result from higher and lower growth rates.

What people are really reacting to is the fact that if you happen to have been the last one to harvest a plant, and then by the natural process of hibernation it dies off, `LASTHARVEST` still blames you. And that's true even if you harvested exactly one herb on peak month and then by sheer happenstance the plant died off during its hibernation month anyway. It's your name, so it's your blame.

Thus, CrystalHerbs now offers the command `CHARVEST HIBERNATION OFF` (and defaults to that). During hibernation month, a herb's threshold is automatically adjusted to 999 when this is set to `OFF`. This means you can keep using any aliases you have to harvest, and the specific hibernating herb is simply skipped. (Of course if that was the only herb you were harvesting, it'll mean it zooms through the rooms instantly.) This may slightly reduce the chance of you getting the blame, but if you happened to harvest to 12 the month before hibernation, and then the randomness of herb growth and die-off conspire to give you bad luck, you could still show on `LASTHARVEST` as the 'culprit'. But at least you will be able to say "I didn't harvest on hibernation month" and maybe they'll believe you.

## Reporting ##

You can check the current threshold for any herb with `CHARVEST INFO <herb>`, or all of them with `CHARVEST REPORT`. This lets you see how your target level calculations are working and know whether it's worth the trouble to slog all the way to wherever that herb grows.

# Replanting #

Any time CrystalHerbs tries to harvest a plant and it's not there at all, the whole thing comes to a total stop and you get an alert notification and sound. (One of the reasons you can't just walk away while it harvests -- the other one being the rules, as noted above.) Typically, this happens for one of three reasons.

1) You specified a path that actually led you to a room of the wrong terrain type. Oops! Get back to the right terrain, set a new path, and continue. This can also happen if one of your move steps failed for some reason.

2) Something moved you. Rocs, gravediggers, sandworms, enemy combatants, etc. (Or, if you're idling and get caught, the admins! Again, that's on you. If you become a shrub, maybe someone else will be harvesting you!) Often the best thing is to go back to the start of your path (once you've dealt with those pesky rocs, that is), reset the path to the same thing it was when you started, and start it up again -- CrystalHerbs will blaze through the places you already harvested since they're already harvested, then resume where it was interrupted.

3) Someone else strip-harvested, or a fire ruined the plants, or for some other reason, a plant that should be there is not there. Now it's time for you to be responsible and make sure that things are there for the next person.

That is, it's time to replant. At this time, CrystalHerbs does not try to help you with this, because there are just too many variables about where the nearest viable source of the plant is. Here's what you need to do:

* `INR EVERYTHING` in case you have any herbs in hand (normally CrystalHerbs will `INR` as it goes but best to be safe)
* Find a nearby room with a few of the plants in question (often moving ahead on your path is the best way, but note that if one room is clear, several may be)
* From here, be prepared to move quick as you will be on a timer
* `HARVEST <plant>`
* If you get a message about getting more than one, `SPLIT <plant> INTO GROUPS OF 1`
* Move back quickly to the strip-harvested room
* `PLANT <plant>`
* Repeat for any other strip-harvested rooms you found along the way
* Go back to the room you first got stopped in
* Make sure you are on balance
* Repeat the `CHARVEST` command you originally did (e.g., `CHARVEST ROSEHIPS`) to re-start harvesting

Again, CrystalHerbs will just pick up where it left off, skipping that plant since it will now see 1 which is below your target (it had better be!).

This also means CrystalHerbs will not allow you to ignore the missing plant, be irresponsible and selfish, and just continue harvesting. This is deliberate. If you really want to be the jerk, now you will have to manually move yourself to the next room, set a new path, and start over. Which probably takes longer than replanting. So just do the right thing, okay?

Except, of course, if it's hibernation season for that plant, so you literally can't replant. Well, sucks to be you. Maybe you should stop harvesting this herb this month. Just do `CHARVEST` again with a list of herbs that doesn't include that plant, and you can pick up where you left off. Otherwise, type `CHARVEST NEXT` to assume this room is done and move on.
