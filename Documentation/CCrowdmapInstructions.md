__This package is in beta-testing.__ It's working for me in everyday life, but I am sure there are challenges I haven't run into yet, that I need to make the package handle once I have seen them. And there's functionality that I've built the core logic for but not yet the specifics just because I don't have the relevant skills or artefacts (see Future Plans at the bottom). Please report problems, with the specific circumstances and text, to Kilian by message, or to freesia@foobox.com by email with logs or screenshots, to help me make it better.

# Purpose #

In Lusternia, if you `WALK` or `PATH TRACK` to somewhere, the server-side routing is smart enough to use almost all your skills and artifacts to get you there by the most efficient means possible. (With a few minor exceptions.) One of the many ways a step into Achaea feels like a return to the dark ages is that, while Achaea does have `WALK` with pathing, it is determinedly dumb pathing; it ignores not only your skills but the artefacts you paid good credits for.

But through the use of the [Crowdmap](https://github.com/IRE-Mudlet-Mapping/AchaeaCrowdmap), this package helps get you there in speed and style, with a minimal footprint on your system (it only does what it does and doesn't take over anything else), and compatibly with any other packages you might want to use.

# Revealing Hidden Exits #

As you walk into rooms, CrystalCrowdmap will use the crowd map data to reveal exits that are not present on the Nexus map, but present. For instance:

`[CrystalCrowdmap] Exits not shown on map: N, ENTER GRATE`

This is deliberately a lightweight display -- no attempt to take over or replace your existing Nexus map, just providing the info you might need to realize, hey, maybe that quest thing you can't find is over here.

Note that the data being used for this is crowdsourced. Sometimes you'll see a note about an exit that's not working for you. That might be because the crowdmap is wrong (and at this time, CrystalCrowdmap has no mechanism for you to correct it; maybe one day!), but in some cases it's because the exit appears and disappears, or becomes accessible and inaccessible to you, based on quests or other game factors.

# Revealing Wormholes #

If you have a wormhole skill (e.g., Serpent) or you own a vibrating or oscillating stick, you can configure (see below) CrystalCrowdmap to both show and use wormholes. In this case, as you walk into a room, you may see a message like one of these:

`[CrystalCrowdmap] Wormholes present: Approaching a stone fortress [Manara Burrow] (v18878)`

`[CrystalCrowdmap] Wormholes formerly present but perhaps missing: In the Granite Hills [Granite Hills, the] (v9127)`

A note about the latter: if at any time you, or CrystalCrowdmap, attempt to traverse a wormhole that the crowdmap says is there, and it isn't, CrystalCrowdmap adds it to a list of defunct wormholes. Maybe someone destroyed it, and if so, maybe it'll get recreated or maybe it won't, and maybe the crowdmap will be updated accordingly and maybe not right away. So CrystalCrowdmap just puts the missing wormhole on a list for a week. When a week passes, if the crowdmap still shows the wormhole is present, CrystalCrowdmap will again try to display and use it, and if it's not there again, well, bang, it goes right back onto the missing wormholes list. This all happens automatically.

# Finding Rooms #

You can do a search of all the rooms in the crowdmap for certain text with `CCM FIND <text>`. The resulting display will make links out of the room numbers that can be clicked on to do pathing as noted below. The search is only on the room name (since the crowdmap doesn't have full room descriptions).

# Finding Paths #

You can ask CrystalCrowdmap to give you a path to a location with `CCM PATH <room>`. If a path can be found it will be displayed. CrystalCrowdmap uses a reasonably smart, cost-driven, breadth-first-search, which is to say, it will generally find the best possible path from your current location to the specified destination, given your configuration and the crowdmap data. It will find paths to farther destinations (150 steps!) than the server-side pathing, too.

By itself, CrystalCrowdmap expects you to provide a room number (in the form `12345` or `v12345`). However, CrystalCrowdmap integrates tightly with CrystalWaypoints. This lets you specify the client-side equivalent of personal landmarks (only with no limit to how many), and then `CCM PATH <waypoint>` automatically uses the waypoint data. CrystalWaypoints will also, if it sees you use CrystalCrowdmap, make the links in your `WAYPOINT LIST` link to CrystalCrowdmap pathing instead of the normal server-side pathing.

Pathfinding will be smart enough to use your configuration (see below) to know what special kinds of movement to use to trim the time down even farther.

# Walking Paths #

But you really don't want to be told a path, do you? You want to actually go there. `CCM GO <room>` supports the same ways to name the destination as above, and finds and shows the path the same, but then it actually walks you there. In my testing, this not only introduces no delays relative to the server-side pathing, it is often slightly faster even if you're not using special skills; it seems server-side pathing deliberately introduces small delays to avoid the hasty message, but CrystalCrowdmap handles that with less delay.

There are a number of things that can go wrong, which CrystalCrowdmap can automatically try to solve:

* If you hit a hasty message, it is automatically dealt with, invisibly.
* If you hit fog, the move is automatically retried. (I will be adding rubble too as soon as I find some to test on!)
* If your path attempted to use a wormhole that's not there, after it's added to the missing wormhole list, CrystalCrowdmap can reroute you automatically, if so configured.
* If you get moved to a point farther along your route somehow, you'll simply continue from there.
* If you get moved to a point earlier on your route somehow, you'll simply resume from there.
* If you get moved to somewhere off your route, you will be noted as lost, and CrystalCrowdmap can reroute you automatically, if so configured.

If you are moving and want to stop, `CCM ABORT` will stop you in your tracks and clear the path.

When using CrystalWaypoints with the `WALK` command, if you have CrystalCrowdmap, you can add `NOCCM` to tell it not to use it for the walk (for instance, `WALK FISHING NOCCM`).

# Actions On Arrival #

You can use `CCM ARRIVAL <bal> <cmd>` to specify a command that will be executed once you arrive. `<bal>` here would be whichever kind of balance that the action should consume, and can be `bal`, `eq`, or `none`. For instance, you can start walking to the fishing shed, then queue up selling the fish to happen automatically on arrival. `CCM ARRIVAL CLEAR` will clear the command in case you changed your mind.

# Configuration #

The following configuration entries determine how CrystalCrowdmap works, and can be viewed with `CCM CONFIG` and changed with `CCM CONFIG <field> <value>` (use a single word for the <field> part, e.g., `CCM CONFIG WormholeUsed YES`):

* **Wormhole Used**: Set this to Yes if you have wormhole skills or a vibrating or oscillating stick. Determines whether wormholes are shown, and whether they are used in pathing. Note that the pathing does account for how wormholes take longer than walking.
* **Dragon Gare Used**: Set this to Yes if you want to route through piercing the veil when in dragonform. Note that CrystalCrowdmap will only do this if you are already in dragonform, and will not transform you. It's safe to turn this on if you are not yet a dragon, because it'll never come into play, until you are.
* **Portal Used**: There is a portal between Sea Lion Cove and the Siroccian Fortress, but not everyone can use it. I do not yet know who can, only that I can't. So this controls whether that portal is used in pathing.
* **Reroute When Lost**: If you are off route, automatically choose and walk a new path to your destination, if this is turned on. Note that in extreme cases this can get you into a loop (which is much like the one the server-side pathing can get you into) and `CCM ABORT` can get you out of it.
* **Reroute On Missing Wormhole**: If you have set Wormhole Used to Yes and CrystalCrowdmap tries to move through a wormhole that turns out to no longer exist, automatically generate and follow a new path (not using that wormhole).
* **Gold Stored In**: If you take a route that uses a ferry, this specifies the container that you can get gold from to pay the fee and put back into after.

The following configuration entries are also available, but are not presently used, because they depend on artefacts that I don't have so I can't set up the code to properly use them. But one day!

* **Dash Used**: With the Anklet of Dashing, use dashes to cover long strings of continuous rooms.
* **Eagle Wings Used**: Use a pair of eagle's or atavian's wings to go to the Duanathar clouds. Works on Sapience and Meropis.
* **Atavian Wings Used**: Use a pair of atavian's wings to go to the Duanatharan clouds. Works on Sapience and Meropis.
* **Island Wings Used**: Use a pair of seafaring talisman island wings to go to the Duanatharic clouds.
* **Knocker Used**: Use a macabre door knocker to go to the underground hubs. Works on Sapience and Meropis.

CrystalCrowdmap assumes you're not lying to it, so don't turn on a capability to use a skill or artefact if you don't have it, or you're going to find your path runs aground in a way CrystalCrowdmap can't recover from.

# Limitations #

There are a small number (less than ten as of this writing) of special, unusual exits in Achaea that the crowdmap handles by built-in Mudlet scripting. At present, CrystalCrowdmap handles only the simplest of these (those that just use sendAll to send multiple simple commands), and ignores the other exits (so does not use them in pathing).

# Future Plans #

Things I hope to do in CrystalCrowdmap in future versions:

* A lot more testing so I can make it recover intelligently from more things that can go wrong. Your bug reports might help.
* Support the other artefacts noted above (dash, wings, and knockers), assuming I either get them, or at least get to borrow some for long enough to do programming and testing.
* I'd love to find out why that portal swirls at my unwanted intrusion, and make CrystalCrowdmap smarter about it.
* I may do some more optimizations as I see places where the performance can be improved.
* Maybe I can integrate with updates to the Crowdmap so I can notify it when wormholes are missing. Ideally I would also send in updates when new wormholes exist, too, though that might depend on having something more efficient than just checking in every room.
* I'd love to tap into the NPC database that other packages use and make this package able to route you to NPCs too, but I am not sure if that database would be available for me to access.
* Maybe I can implement a few more of those Mudlet-scripted oddball exits.
