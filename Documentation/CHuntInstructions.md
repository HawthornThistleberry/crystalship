# Introduction #

CrystalHunt automates hunting (only within a single room, and you must of course follow all AFK rules) by creating and maintaining target lists (possibly from saved lists you've made in advance), cycling hunt attacks, handling razing, panicking if things go badly, collecting corpses and drops, and coordinating with squads and groups on hunts. It is highly configurable to suit your preferences, weapons, attacks, and skills, and can even adapt automatically to skillflexing. It uses its own target aliases (but is still building up its list of aliases -- see the section About Aliases to learn the limitations, the reasons for them, and how you can help). _Please read through these instructions at least once to get CrystalHunt working for you._

**Does CrystalHunt kill loyals?**: If you are having trouble with CrystalHunt killing loyals, there are a few small changes to the configuration you need to make. In previous versions I didn't make them the default, and didn't explain them as well as I could, but it was always possible to avoid killing loyals (and in fact, CrystalHunt goes an extra step by maintaining a global list of known loyals to avoid attacking no matter what). It's best to read and understand the whole document, but here's the tl;dr version:

* Use `$` in the setup of your attacks instead of `@`.
* If you're still having trouble, use BHUNT instead of HUNT or MHUNT.
* If you're still having trouble, ask Lendren for help.

I'd rather we all got it to work. For you, I don't want you killing loyals, or getting your loyals killed. For me, I work hard on making this package and I don't want it to get a reputation as being broken when there's so much effort into avoiding this very thing.

# Setup #

Before you start using CrystalHunt to hunt, you need to configure a handful of things.

## Prerequisites ##

* CrystalNotices must be installed.
* CrystalQueues must be installed and set up (see **CQueuesInstructions.md**).
* If CrystalDefenses is installed, CrystalHunt will know if you have acquisitio, avaricehorn, and/or yoyo up, and skip the appropriate drop collections.

## Commands ##

First, you _must_ specify a set of commands:

* `CH ATTACK`: Your attack command: by default this will be `ATTACK $` which lets the MUD pick an appopriate skill.
* `CH BEFORE` and `CH AFTER`: If desired, something to do just before each target, and just after.
* `CH RAZE`: A command to use for razing shields. Use the special command `RAZE` to automatically use either Nullify or Void depending on which kind of magic you use (adjusts as you skillflex). Use `NONE` to ignore shields and just keep attacking, useful if you have a Rune of Razing. Use `WAIT` to make CrystalHunt stop doing anything when they raise shields and then attack the moment it gets lowered.
* `CH PANIC`: A command to do when panicking -- that is, being too low on health. The special command `SHIELD` will use either Pentagram or Circle, depending on your type of magic. The special command `RUN` will attempt to run in a random direction (from available exits), and if it can't find any exits, it will try to fly or burrow.
* `CH BEAST`: A command to have your beast do when you and your beast are both on balance. Set to `NONE` if your beast doesn't attack, or perhaps to `BEAST ORDER ATTACK $` to use a normal attack, or some other beast attack that works on mobs.
* `CH BEASTHEAL`: If your beast has Heal Health, turning this on means it will ask for beast healing any time your health is below 75% and you have beast balance; this takes priority over the attack noted above, if it's turned on. Thus, if you set the above to an attack and this to on, your beast balances will be used on heals if you are below 75% and attacks otherwise. (The 75% threshold is used because beast heals do not consume any potions or anything but beast balance, so might as well use them aggressively.)

The low and high magic special commands should automatically track as you change skillsets.

To set a command, the syntax is `CH <command> <balancetype> <command>`. The balancetype is typically `eq` or `bal`, whichever this command consumes; omit this for the `BEAST` attack since it's always beast balance.

### Target Types ###

In all of these, you can insert the following special characters to refer to the target in different ways. *Your choice of which one to use is key to making sure you balance getting the best out of gnomeweapons with not targeting loyals.* The options are:

* `@`: The name, only, of the target, as it appears in IH (e.g., `scorpion`)
* `#`: The ID number of the target (e.g., `332521`)
* `$`: The alias of the target (e.g., `astralbeast`)

Which should you choose? That may depend on your specific skills, but for most people:

* If you use a gnomeweapon use `$`.
* Otherwise use `@#`.

Why? With anything but a gnomeweapon, `@#` ensures you attack precisely and exclusively the creature you are targeting. Which is usually the safest and best thing to do. But with a gnomeweapon, this would waste the extra attacks if you kill the creature midway through the up-to-nine attacks it's doing. In this case, you could use `@` but this is dangerous: if you are attacking a `bird` that could turn out to hit some other bird that's there, including a loyal. So for a gnomeweapon, use `$` to use the alias `astralbeast`.

See the section below about Aliases to understand the gory details, if you want to, about why CrystalHunt does this aliasing/unaliasing. (But tl;dr: there _are_ good reasons, and they're mostly to do with limitations in Lusternia's GMCP data.)

### Command Examples ###

* `ch attack eq fire pen $` (because you have a gnome weapon)
* `ch attack eq play wildechord @#` (because you don't yet)
* `ch raze eq raze` (to use the low or highmagic version)
* `ch panic eq invoke serpent` to use Serpent when you're in a panic
* `ch before bal rub crown` if you had some enchantment that was good to rub just before starting on each target
* `ch beast beast order attack $` to make your beast help you out with attacks

## Other Configuration ##

Other things you might want to set:

* `CH THRESHOLD <amount>` to set the threshold of health at which you should panic. Defaults to 30%.
* `CH COLLECT ON` to have CrystalHunt automatically get corpses, essence, and gold after each kill. Note: if you are using CrystalDefenses, and you have Acquisitio, Yoyo, or AvariceHorn defenses up, CrystalHunt automatically skips the associated collects for corpses or gold, even if `COLLECT` is on, but still tries to get essence regardless. That way you can just leave this on and still avoid duplicative grabs.
* `CH EXCLUDE <target>`: You know that problem where you're hunting peacocks with your own pet peacock, or your best friend's peacock, following you, and disaster results? Specify the protected peacock's full ID (e.g., peacock12345) as excluded, and CrystalHunt will automatically avoid attacking it even when you tell it to attack peacocks. This could also include that one really big monster that's in the same room with a bunch of smaller ones, or anything else you want to be sure never to attack. `CH INCLUDE <target>` to restore a target to huntability, and `CH EXCLUDED` to see the list of what you've excluded. Note that there is also a global exclusion list that you don't have to maintain -- ask Lendren if you want your pet added to it.
* `CH CALL <cmd>|OFF`: If this is set to a command, that command will be used to call targets to others who are hunting with you, typically via an aether. You will almost always want the command to look like this: `CH CALL squadt Target: @` because that is the _de facto_ standard for calling targets. The only part you should change is the `squadt` part which you might change to a clan or coven, or heck, maybe even says. Whatever command you use should definitely not require or consume any balances. This will fire on each new target (which means if you have six cows in the room, it'll call on each one). You can use `@`, `$`, and/or `#` as above, though most other hunters will expect only `@` or `$`.
* `CH ANSWER ON|OFF|IDLE`: When turned on, if someone on a clan or other aether calls a target in the standard `Target: <target>` format, CrystalHunt will automatically start hunting it. If you choose `IDLE` this happens *only* if you're not already hunting something -- so you won't switch targets, only start new ones. But if it's `ON` it will switch targets.
* `CH GAG ON|OFF`: When turned on, critical hit lines are gagged. Seems like something you might not want, but when a group of people are hunting together with gnomeweapons, it's amazing how much spam the crits produce, and how much nicer it is without them. Defaults to off.

You can use `CH STAT` to see the status of your hunting, including most of your configuration.

# Hunting #

With these things set, you're ready to hunt. Make your first time against something harmless, so if you set up a command badly, you won't be in too much trouble while you do things manually and figure it out. The commands for basic hunting are as follows, but note that `MHUNT` is the one you'll likely use most often.

* `CH HUNT <target>`: Hunt one of a single target. You can specify a general target (e.g., `stag`) or a specific target (e.g., `stag12345`).
* `CH BHUNT <target>`: As `CH HUNT` but it hunts blindly -- it attacks whatever you say without verifying that it's there. If it's not, you're going to get spammed, so be ready to `CH STOP NOW`. This also ensures that it only aims at the word you specify, which means if you're on Astral and there are loyal eagles there alongside the enemy ones, you will attack them if you `CH BHUNT EAGLE`; but by the same token, if you `CH BHUNT ASTRALBEAST`, CrystalHunt won't know how to track how many there are as they enter and leave the room and are killed, but it will also never attack that eagle, even if you haven't set up your target types right.
* `CH MHUNT <target> <target>...`: Hunt all of one target, then all of another, and so on, until the room is cleared. Specify only target types (e.g., `stag`) which will match against all present. You can specify just one target type (e.g., `CH MHUNT MITRAN` to hunt all the mitrans in the room) or multiple targets (e.g., `CH MHUNT CAVE-FISHER MANTEKARR ADORATH PHYCOMID CENTIPEDE SOLIFUGID URCHIN ROCKEATER BEETLE SPIDION BRAINEATER ASCONOID BASIDION` when clearing out the Undervault tunnels). CrystalHunt will hunt them in the order you provide, *so put the aggro targets earlier*. Also, if a particular target drops out of other targets in the area (e.g., in the Tar Pits), list it too -- probably first. For instance in the Tar Pits, `CH MHUNT BLOB BUCK BADGER FOX` ensures that every time a blob drops out of something else, you hunt the blob next, then go on down the list.
* `CH ADD <target> <target>...`: Add target(s) to the list (e.g., when they enter the room). Note that, if you've provided a list via `MHUNT`, and another one of those creatures enters, it'll be added automatically. If you provide targets with `ADD`, either provide full names (e.g., `peacock12345`) or provide a generic alias -- but CrystalHunt will _not_ scan the room for all matches to the noun, as it does with `MHUNT`. This is rarely used because the target you already specified in the `MHUNT` will automatically add and remove as they enter and leave; you would only use this if, for instance, you specified a list of targets and then realized you missed one.
* `CH REMOVE <target> <target>...`: Remove target(s) from the list (either ones you added with `ADD`, or those added by `MHUNT`, though in the latter case, you must provide the full ID). Note that creatures leaving the room are automatically removed. Also rarely used.
* `CH STOP`: Finish the current hunt, then stop.
* `CH STOP NOW`: Abort a hunt in progress. It's up to you to do something next since your foe will be angry.
* `CH STOP RUN`: Abort a hunt in progress, then automatically invoke your panic command. Good if, for instance, you're not yet hurting but six more obesefessors just walked in and you better split while you can.

## Saving And Using Hunt Lists ##

CrystalHunt offers two ways to save a hunt list for later re-use: by area, and by name. `CH LISTS` will display the lists you've saved by both methods.

### By Area ###

`CH AHUNT <target> <target>...` works exactly the same as `MHUNT`, with the additional detail that the list gets saved and associated with the current area you are in.

In this case, 'area' means whatever Lusternia identifies as an area, which is not always what you'd expect -- for instance, different parts of the mountains are different areas -- and in a few cases, may not always match what the map shows (e.g., all of the Serenwilde is one area, but north and south show different titles in the map). The Undervault tunnels are multiple areas too.

Notwithstanding this limitation, this can be very useful because often in a given area you're always hunting the same targets. For instance, in the Razine Tunnels of the Undervault:
`CH AHUNT CAVE-FISHER MANTEKARR ADORATH BRAINEATER PHYCOMID CENTIPEDE SOLIFUGID URCHIN ROCKEATER BEETLE SPIDION ASCONOID BASIDION`

Once you've saved a list this way, you can run it very quickly:
`CH AHUNT`
to hunt everything on that list again. (This might be good to put on a Nexus button or keybind.)

CrystalHunt also supports using AHUNT in a slightly more automated way. `CH ROOMHUNT ON` turns on a mode where CrystalHunt will do a silent AHUNT on every room entry. Naturally this won't work if you don't already have AHUNT lists set up in the area you're hunting in. `CH ROOMHUNT OFF` naturally turns this off.

### By Name ###

Saved names by area are all well and good, but maybe you need more than one list. In that case, you can define your own names for lists.

`CH LHUNT <name> <target> <target>...` again works the same as `MHUNT` but saves the list with the name you gave. The name should be a single word made of letters, numbers, underscores, and dashes -- no spaces, and no weird punctuation.

Once the list has been saved:
`CH LHUNT <name>` will hunt that list just as if you'd done the equivalent `MHUNT` command.

## Roomhunt ##

`CH ROOMHUNT ON` (or the button on the CrystalHunt panel) turns on a mode where CrystalHunt silently looks for targets from the area's AHUNT list on every room entrance, and if so, hunts them. Turn this on, then simply use your movement keys to move around the area, to hunt efficiently. Note: it takes a little time to scan a room; if you zip through rooms too quickly, CrystalHunt will get out of synch and you may see some of its otherwise-silent scanning, or it might miss a target. Just slow down - about a half second per move will do nicely. 

## The CrystalHunt Panel ##

CrystalHunt will create a panel that shows you the current status of your hunt and updates in real time (mostly); it also includes a few buttons. Here's an example of what it might look like:

![CrystalHunt panel](https://bitbucket.org/HawthornThistleberry/crystalship/raw/dfad75323fc488d8fdff548ce632c2ed845bb313/Documentation/CHPanel.png)

At the top you'll see your current target and its health, with a shield icon if it's shielded; its health also shows as a red bar. If you have more targets on a hunt list, it'll show them. Then the buttons are for varying levels of stopping: Stop means stop after this target, Now makes it stop immediately, and Run makes it stop and then invoke your Panic command. If you're not in a hunt, you'll instead see a message to that effect and an AHunt button that will do an AHunt.

Next, a row shows the current status of Roomhunt with a button to toggle it on and off.

Below that, if you are not already a demigod, you'll see a probably very depressing green progress bar showing your overall progress to demigod, from level 0 to level 100. This is based on a set of experience point counts that I was given in Achaea, so it's possible that it's inaccurate if Lusternia changed those values, but so far it seems to be about right. (If not, I'll find out eventually!) Once you become a demigod, this vanishes; maybe there's something else I could put here? Contact me with ideas, provided we have the data in GMCP or elsewhere. Speaking of GMCP, sadly, Lusternia's GMCP character info does not include the percentage you see in QSC like Achaea's does, so this will only update when you do QSC. Short of convincing Lusternia's devs to add that value into GMCP, that's the best we can do.

Finally, at the bottom, a shortened version of the CrystalQueues queue list. This is handy in case some other queue entry has slipped into the list. For instance, suppose you're using CrystalHerbs to harvest sparkleberry when an abhorrence walks in. Due to how CrystalQueues works, your system will automatically pause harvesting, kill the abhorrence (assuming you set up an autohunt, about which see the next section, or you just hit a key that does an AHUNT), and then quietly resume harvesting where it left off. When this happens you can see the queue entries neatly lining up, appearing, and disappearing.

## Setting Up Autohunts ##

You might want to make triggers (in your own package, not inside CrystalHunt) that automatically start hunts on certain situations, like

* being pulled into a gravedigger pit
* having an astral beast walk into the room
* meeting an aggressive enemy like a cave-fisher

The best thing to do this case is simply run the appropriate commands:

```javascript
send_command('ch mhunt fisher',0);
```
You can't just let Nexus send the command or it might not find the alias (though in recent versions it seems maybe it will, but I'm not sure enough to advise this yet); you need to use Execute Script and a `send_command()` with the `,0` option to ensure that your triggers runs the alias instead of just sending a command to the MUD.

Of course if you're already hunting a list, and this fires, it'll replace the list, so in some cases you might want use `CH ADD` instead. But it's better to have already put cave-fishers into the list, because then CrystalHunt will watch for their arrival and add them in automatically (though not at the front of the list).

In fact, if you've saved area hunt lists, you might find it much better to do this:
```javascript
if (get_variable('sCHCurrentTarget') == '') send_command("ch ahunt", 0);
```
That way if you are already hunting it won't interrupt or reset it, and if you aren't, it'll automatically hunt the fisher and anything else in the room you would usually hunt.

# About Aliases #

Please read this if you want to understand how and why CrystalHunt is doing what it's doing to make target lists, and particularly about aliases.

When you're just targetting things by hand, on the Ethereal Plane you might `ATTACK ETHEREAL`, or on the Astral Plane you might `ATTACK ASTRALBEAST`. Great for making hunting simple, with a big side benefit of not targeting people's loyal eagles and peacocks. This works through what the MUD calls an alias: if you `PROBE PEACOCK` you will see this line:
```It has the following aliases: peacock, ethereal, white.```
Whereas if you probe someone's loyal peacock, you won't see `ethereal` on the list. That's why `ATTACK PEACOCK` will hit both (and lead to you accidentally killing Amirae's peacock) but `ATTACK ETHEREAL` will only attack the ethereal ones.

Unfortunately, the list of aliases is not easily revealed by the MUD in a way that CrystalHunt, or any other hunting system, can capture. The only definitive way is to spammily probe everything in the room. You can get 99% of the benefit by building up an enormous database of the "long descriptions" you see in `IH`, assuming that there's never such a thing as a loyal peacock with the same description as the huntable ones, but I don't have such a database, and no one who has one for Mudlet is sharing. It would be very simple for Lusternia to make this available by GMCP, but they don't even make the noun (e.g., "peacock" available), let alone all the aliases.

This leaves CrystalHunt (and all similar packages) two main approaches, each with its pros and cons.

**BHUNT**: The `CH BHUNT` command does a 'blind hunt' where it just attacks whatever noun you give it. This works fine with aliases. However, it is totally incapable of recognizing when the thing you're trying to target isn't there, because the only indication of that the MUD offers is a randomly selected message like `You cannot see that being here.` which are also used for a thousand other things. End result: if CrystalHunt used that trigger to abort a hunt, every time you tried to release a kirigami and were out of them, or touched something that wasn't there, etc. it would abort the hunt -- possibly while something is still attacking you. Because it can't possibly know if that not-here message came from its attack or something else. That's how it used to work, in fact, and that flaw led to people dying in hunts. Not good. But if it doesn't abort the hunt when the target isn't there, it just spams its attack forever, which is also not good. The current version of CrystalHunt tries to split the difference: four not-here messages in quick succession cause it to abort. Still not ideal.

**MHUNT**: All the other attacks, notably `CH MHUNT`, use `IH` to make a list of targets to attack, and then use GMCP (messages the MUD sends behind the scenes automatically) to update that list. This has a lot of big benefits. CrystalHunt can know exactly how many of each target there is, can update that list dynamically as creatures wander in and out of the room, can accurately update things when someone else in your hunting group kills a target, can prioritize targets in an order you specify (e.g., focus on the aggro cave-fishers, then work on spidions when they're done), and can still target by noun to allow multi-attack weapons like gnomeweapons to spill over attacks from one target onto another. But the one big disadvantage is that when you look at `IH` if you're looking for `ETHEREAL` you won't find it. Instead, you just see a lot of `PEACOCK`s, including the loyal ones.

CrystalHunt uses two tricks to try to mitigate the consequences of this disadvantage.

* **Aliases**: While I don't have access to the huge databases of huntable creatures that other packages have built up over the years, I do have a mechanism where I can build up a list of aliases myself, with your help, which all users of CrystalHunt share. Thus, if you `CH MHUNT ETHEREAL` it knows to look for peacocks, opossums, and stags already. When you encounter ones where it doesn't work, just send to Lendren what the target you use is, and then the list of the nouns that show on `IH` that it should target. Your copy of CrystalHunt updates the list of aliases it's using every time you log in and every time you `CINSTALL HUNT`, so I can update that without you having to reinstall. By using `$` instead of `@`, the actual attack will end up pointing at the original alias, which will still carry over when you use a gnomeweapon, but also avoid hitting unintended targets.

* **Exclusions**: Similarly, while you can `CH EXCLUDE` a target by name and number, I maintain a universal list of exclusions which are updated the same way, so as I find out about loyal peacocks and eagles, I can add them to the list. Just let Lendren know the IH listing for your loyals and I can add them.

These are obviously imperfect solutions. I am noodling on some improvements. And I'm always hoping that Lusternia will consider some suggestions to add nouns and aliases to GMCP information so we can finally be rid of the entire tradeoff and just have a definitive and thoroughly usable list. But ultimately, this is a workable approach once the aliases list is built up. When you run into aliases that don't work, instead of grousing about it, just send me a message, and it'll be fixed in short order (often within minutes).

# Future Plans #

Upcoming versions of CrystalHunt will include the following features:

* An option to empower before killing, to increase experience gain.
* Someday, assuming I find a suitable free cloud-based database I can use, I hope to build a proper short_desc-to-alias lookup table to make up for the conspicuous gap in GMCP data. (Right now, if you have specified "stag", CrystalHunt knows not to target "a staggering zombie" or even "a stag-horned beetle", but it could miss "an ur'stag" and would be fooled into attacking "a dragon with antlers like a stag". Maintaining an always-out-of-date table of thousands of short_descs seems like a poor solution, but unless they finally give us nouns or aliases in GMCP, it's the only one.)
* Provide a better method for adding entries to the aliases list, using the same cloud-based database solution.
